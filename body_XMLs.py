import base64
from datetime import datetime
import time
import json
import server

import sxr_decode
import tcp_logger


with open('C:/Introscopy_server/dist/config.json', 'r') as config:
    file_d = config.read()
data_json = json.loads(file_d)
host_ip = data_json['ip_adress']
host_port = int(data_json['port'])
serial_number = data_json['serial_number']
metal_wait_time = int(data_json['metal_wait_time'])
if metal_wait_time == 0:
    metal_wait_time = 3600
rad_wait_time = int(data_json['rad_wait_time'])
if rad_wait_time == 0:
    rad_wait_time = 3600
bomb_wait_time = int(data_json['bomb_wait_time'])
if bomb_wait_time == 0:
    bomb_wait_time = 3600

image_gun_path = r'C:/Introscopy_server/dist/resources/intell_gun.bmp'
image_bomb_path = r'C:/Introscopy_server/dist/resources/intell_bomb.bmp'
image_rad_path = r'C:/Introscopy_server/dist/resources/intell_rad.bmp'


def get_int_time():
    return datetime.now().hour * 3600 + datetime.now().minute * 60 + datetime.now().second

def parse_body(topic, picture_file, tcp_conn):
    device = ''
    state = ''
    source = ''
    data = ''
    if 'Introscopy' in topic and \
            (data_json['device'] == 'Introscopy' or data_json['other'] == 1):
        device = 'Introscopy'
        state = 'LimitExceeded'
        source = f'<tt:SimpleItem Name="Id" Value="{serial_number}" />'
        # file = open(picture_file, 'rb')
        
        '''image_main() check if there is a new file in current archive folder
        and return path to that file'''
        new_sxr_path, result_flag = server.image_main()

        '''Convert to BMP if there`s coresponding flag in config.json
        Case BMP == 1: convert .SXR in .BMP, encode in Base64 and insert in data
        Case BMP == 0: encode .SXR in Base64 and insert into data variable'''
        if data_json['BMP'] == '1':
            try:
                sxr_object = sxr_decode.SXR(new_sxr_path)
                bmp_path = sxr_object.convert_to_bmp()
                file = open(bmp_path, 'rb')
                picture = base64.b64encode(file.read(70 * 1024 * 1024)).decode('utf-8')
            except PermissionError:
                print('File Permission Error')
                file = open(picture_file, 'rb')
                picture = base64.b64encode(file.read(1024 * 1024)).decode('utf-8')

        else:
            try:
                file = open(new_sxr_path, 'rb')
                picture = base64.b64encode(file.read(70 * 1024 * 1024)).decode('utf-8')
            except PermissionError:
                print('File Permission Error')
                file = open(picture_file, 'rb')
                picture = base64.b64encode(file.read(1024 * 1024)).decode('utf-8')
        if result_flag:
            result = 'Suspicious object was found!'
        else:
            result = 'No suspicoius objects'
        data = f'''<tt:SimpleItem Name="Account" Value="Admin" />
                <tt:SimpleItem Name="Picture" Value="{picture}" />
                <tt:SimpleItem Name="Result" Value="{result}" />'''

    if 'RadiationMonitoring' in topic and \
            (data_json['device'] == 'Trio' or data_json['other'] == 1):
        device = 'RadiationMonitoring'
        state = 'Detect'
        source = f'<tt:SimpleItem Name="Id" Value="{serial_number}" />'
        try:
            file = open(image_rad_path, 'rb')
            picture = base64.b64encode(file.read(20*1024 * 1024)).decode('utf-8')
        except PermissionError:
            print('File Permission Error')
            file = open(picture_file, 'rb')
            picture = base64.b64encode(file.read(20*1024 * 1024)).decode('utf-8')
        alarm_dict = {'rad_level': 0}
        start_time = get_int_time()
        alarm_dict['time'] = start_time
        # start_time = datetime.now()
        # wait = datetime.
        while alarm_dict['rad_level'] == 0 \
                and get_int_time() <= start_time + rad_wait_time:
            # alarm_dict = tcp_logger.tcp_logger_trio(tcp_conn, 'CC TMK')
            alarm_dict = tcp_logger.global_alarm_dict
            # print('Radiation:', alarm_dict['rad_level'])
        tcp_logger.global_alarm_dict = tcp_logger.reset_alarm_dict()

        print('Radiation:', alarm_dict['rad_level'])
        if alarm_dict['rad_level'] == 0:
            data = f'''<tt:SimpleItem Name="Picture" Value="{picture}" />
                <tt:SimpleItem Name="Account" Value="Admin" />
                <tt:SimpleItem Name="Category" Value="Alarm" />
                <tt:SimpleItem Name="Mesures" Value="No radiation source found" />'''
        else:
            data = f'''<tt:SimpleItem Name="Picture" Value="{picture}" />
<tt:SimpleItem Name="Account" Value="Admin" />
<tt:SimpleItem Name="Category" Value="Alarm" />
<tt:SimpleItem Name="Mesures" Value="Radiation source found: Level {alarm_dict['rad_level']}, Zones {alarm_dict['rad_zone']}" />'''

    if 'MetalDetector' in topic and \
            (data_json['device'] == 'Trio' or data_json['other'] == 1):
        device = 'MetalDetector'
        state = 'Detect'
        source = f'<tt:SimpleItem Name="Id" Value="{serial_number}" />'
        try:
            file = open(image_gun_path, 'rb')
            picture = base64.b64encode(file.read(20*1024 * 1024)).decode('utf-8')
        except PermissionError:
            print('File Permission Error')
            file = open(picture_file, 'rb')
            picture = base64.b64encode(file.read(20*1024 * 1024)).decode('utf-8')

        # alarm_dict = tcp_logger.tcp_logger_trio(tcp_conn, 'CC TMK')
        alarm_dict = {'level': 0}
        start_time = get_int_time()
        alarm_dict['time'] = start_time
        while alarm_dict['level'] == 0 \
                and get_int_time() <= start_time + metal_wait_time:
            # alarm_dict = tcp_logger.tcp_logger_trio(tcp_conn, 'CC TMK')
            alarm_dict = tcp_logger.global_alarm_dict
            print('XML`s Metal:', alarm_dict['level'])
            print('XML`s Global Metal:', tcp_logger.global_alarm_dict['level'])
            # time.sleep(0.5)
        tcp_logger.global_alarm_dict = tcp_logger.reset_alarm_dict()

        if alarm_dict['level'] == 0:
            data = f'''<tt:SimpleItem Name="Picture" Value="{picture}" />
                <tt:SimpleItem Name="Account" Value="Admin" />
                <tt:SimpleItem Name="Category" Value="Alarm" />
                <tt:SimpleItem Name="Mesures" Value="No metal objects found" />'''
        else:
            data = f'''<tt:SimpleItem Name="Picture" Value="{picture}" />
<tt:SimpleItem Name="Account" Value="Admin" />
<tt:SimpleItem Name="Category" Value="Alarm" />
<tt:SimpleItem Name="Mesures" Value="Metal object found: Level {alarm_dict['level']}. Zones {alarm_dict['zone']}" />'''

    if 'SteamDetector' in topic and \
            (data_json['device'] == 'Trio' or data_json['other'] == 1):
        device = 'SteamDetector'
        state = 'Detect'
        source = f'<tt:SimpleItem Name="Id" Value="{serial_number}" />'
        try:
            file = open(image_bomb_path, 'rb')
            picture = base64.b64encode(file.read(20*1024 * 1024)).decode('utf-8')
        except PermissionError:
            print('File Permission Error')
            file = open(picture_file, 'rb')
            picture = base64.b64encode(file.read(20*1024 * 1024)).decode('utf-8')

        # alarm_dict = tcp_logger.tcp_logger_trio(tcp_conn, 'CC TMK')
        alarm_dict = {'explosives': 0}
        start_time = get_int_time()
        alarm_dict['time'] = start_time
        # while (alarm_dict['explosives'] == 0 or alarm_dict['time'] <= start_time) \
        #         and get_int_time() <= start_time + metal_wait_time:
        while alarm_dict['explosives'] == 0 \
                and get_int_time() <= start_time + bomb_wait_time:
            # alarm_dict = tcp_logger.tcp_logger_trio(tcp_conn, 'CC TMK')
            alarm_dict = tcp_logger.global_alarm_dict
            # print('Explosives:', alarm_dict['explosives'])
        tcp_logger.global_alarm_dict = tcp_logger.reset_alarm_dict()

        print('Explosives:', alarm_dict['explosives'])
        if alarm_dict['explosives'] > 0:
            result = 'Traces of explosive substance found'
        else:
            result = 'No traces found'

        data = f'''<tt:SimpleItem Name="Picture" Value="{picture}" />
            <tt:SimpleItem Name="Account" Value="Admin" />
            <tt:SimpleItem Name="Category" Value="Alarm" />
            <tt:SimpleItem Name="Mesures" Value="{result}" />'''

    '''Check for config`s flag if the non-introscopy events should be generated'''
    if data_json['other'] == '1':

        if 'AccessControl' in topic:
            device = 'AccessControl'
            source = ''
            data = '''<tt:SimpleItem Name="Name" Value="AAA" />
                    <tt:SimpleItem Name="Comment" Value="comment text" />
                    <tt:SimpleItem Name="Place" Value="Terminal" />'''

            if '/Accident' in topic:
                state = 'Accident'
            elif '/Fault' in topic:
                state = 'Fault'

        elif 'FireAlarm' in topic:
            device = 'FireAlarm'
            source = ''
            data = '''<tt:SimpleItem Name="Category" Value="Emergency" />
                    <tt:SimpleItem Name="Zone" Value="Zone 1" />
                    <tt:SimpleItem Name="Comment" Value="Comment text" />'''

            if '/Alarm' in topic:
                state = 'Alarm'
            elif '/Fault' in topic:
                state = 'Fault'
            elif '/Fire' in topic:
                state = 'Fire'

        elif 'GasAnalysis' in topic:
            device = 'GasAnalysis'
            state = 'ConcentrationExceeded'
            source = '<tt:SimpleItem Name="Id" Value="1_01" />'
            data = '''<tt:SimpleItem Name="Place" Value="Terminal 1" />
                    <tt:SimpleItem Name="ConcentrationLimit" Value="50 g/m^3" />
                    <tt:SimpleItem Name="GasType" Value="Nitrogen" />
                    <tt:SimpleItem Name="Concentration" Value="100 g/m^3" />
                    <tt:SimpleItem Name="Comment" Value="Comment text" />'''

        elif 'NeutronProbing' in topic:
            device = 'NeutronProbing'
            state = 'Detect'
            source = '<tt:SimpleItem Name="Id" Value="1_01" />'
            data = '''<tt:SimpleItem Name="Place" Value="Hall" />
                    <tt:SimpleItem Name="ExplosiveType" Value="Some material" />
                    <tt:SimpleItem Name="Location" Value="12.342:34.1222" />
                    <tt:SimpleItem Name="Comment" Value="Comment text" />'''

        elif 'VideoSurveillanceSystem' in topic:
            device = 'VideoSurveillanceSystem'
            source = '<tt:SimpleItem Name="Id" Value="1_01" />'
            data = '''<tt:SimpleItem Name="Category" Value="Task" />
                    <tt:SimpleItem Name="Comment" Value="Comment text" />
                    <tt:SimpleItem Name="Priority" Value="5" />'''

            if 'ChannelDisconnect' in topic:
                state = 'ChannelDisconnect'
            elif 'ChannelConnect' in topic:
                state = 'ChannelConnect'
            elif 'RecordTurnOff' in topic:
                state = 'RecordTurnOff'
            elif 'RecordTurnOn' in topic:
                state = 'RecordTurnOn'
            elif 'MotionDetectorTrigger' in topic:
                state = 'MotionDetectorTrigger'
            elif 'ReadRecordRequest' in topic:
                state = 'ReadRecordRequest'
            elif 'DeleteRecordRequest' in topic:
                state = 'DeleteRecordRequest'
            elif 'EditDbRecordRequest' in topic:
                state = 'EditDbRecordRequest'
            elif 'CopyDbRecordRequest' in topic:
                state = 'CopyDbRecordRequest'
            elif 'Authorization' in topic:
                state = 'Authorization'
            elif 'ServerPowerOn' in topic:
                state = 'ServerPowerOn'
            elif 'ServerPowerOff' in topic:
                state = 'ServerPowerOff'

    return device, state, source, data


def resp_get_cap():
    body = f'''<?xml version="1.0" encoding="UTF-8"?>
        <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope" 
        xmlns:e="http://www.w3.org/2003/05/soap-encoding" 
        xmlns:wsa="http://www.w3.org/2005/08/addressing" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl" 
        xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2" 
        xmlns:wstop="http://docs.oasis-open.org/wsn/t-1" 
        xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2" 
        xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2" 
        xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2" 
        xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2" 
        xmlns:wsdl="http://schemas.xmlsoap.org/wsdl" 
        xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12" 
        xmlns:http="http://schemas.xmlsoap.org/wsdl/http" 
        xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery" 
        xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing" 
        xmlns:tt="http://www.onvif.org/ver10/schema" 
        xmlns:tns1="http://www.onvif.org/ver10/topics" 
        xmlns:tds="http://www.onvif.org/ver10/device/wsdl" 
        xmlns:trt="http://www.onvif.org/ver10/media/wsdl" 
        xmlns:tev="http://www.onvif.org/ver10/events/wsdl" 
        xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl" 
        xmlns:tst="http://www.onvif.org/ver10/storage/wsdl" 
        xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
        xmlns:tr2="http://www.onvif.org/ver20/media/wsdl" 
        xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl" 
        xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl" 
        xmlns:axt="http://www.onvif.org/ver20/analytics" 
        xmlns:trp="http://www.onvif.org/ver10/replay/wsdl" 
        xmlns:tse="http://www.onvif.org/ver10/search/wsdl" 
        xmlns:trc="http://www.onvif.org/ver10/recording/wsdl" 
        xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl" 
        xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl" 
        xmlns:pt="http://www.onvif.org/ver10/pacs" 
        xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl" 
        xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl" 
        xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl" 
        xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl" 
        xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl" 
        xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl" 
        xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl" 
        xmlns:ter="http://www.onvif.org/ver10/error">
        <s:Header></s:Header>
        <s:Body>
            <tds:GetCapabilitiesResponse>
            <tds:Capabilities>
                <tt:Analytics>
                    <tt:XAddr>http://{host_ip}:{host_port}/onvif/analytics_service</tt:XAddr>
                    <tt:RuleSupport>true</tt:RuleSupport>
                    <tt:AnalyticsModuleSupport>true</tt:AnalyticsModuleSupport>
                </tt:Analytics>
                <tt:Device>
                    <tt:XAddr>http://{host_ip}:{host_port}/onvif/device_service</tt:XAddr>
                    <tt:Network>
                        <tt:IPFilter>true</tt:IPFilter>
                        <tt:ZeroConfiguration>true</tt:ZeroConfiguration>
                        <tt:IPVersion6>false</tt:IPVersion6>
                        <tt:DynDNS>true</tt:DynDNS>
                        <tt:Extension>
                            <tt:Dot11Configuration>true</tt:Dot11Configuration>
                        </tt:Extension>
                    </tt:Network>
                    <tt:System>
                        <tt:DiscoveryResolve>true</tt:DiscoveryResolve>
                        <tt:DiscoveryBye>true</tt:DiscoveryBye>
                        <tt:RemoteDiscovery>false</tt:RemoteDiscovery>
                        <tt:SystemBackup>true</tt:SystemBackup>
                        <tt:SystemLogging>true</tt:SystemLogging>
                        <tt:FirmwareUpgrade>true</tt:FirmwareUpgrade>
                        <tt:SupportedVersions>
                            <tt:Major>17</tt:Major>
                            <tt:Minor>12</tt:Minor>
                        </tt:SupportedVersions>
                        <tt:SupportedVersions>
                            <tt:Major>2</tt:Major>
                            <tt:Minor>6</tt:Minor>
                        </tt:SupportedVersions>
                        <tt:SupportedVersions>
                            <tt:Major>2</tt:Major>
                            <tt:Minor>4</tt:Minor>
                        </tt:SupportedVersions>
                        <tt:SupportedVersions>
                            <tt:Major>2</tt:Major>
                            <tt:Minor>0</tt:Minor>
                        </tt:SupportedVersions>
                        <tt:Extension>
                            <tt:HttpFirmwareUpgrade>true</tt:HttpFirmwareUpgrade>
                            <tt:HttpSystemBackup>true</tt:HttpSystemBackup>
                            <tt:HttpSystemLogging>true</tt:HttpSystemLogging>
                            <tt:HttpSupportInformation>true</tt:HttpSupportInformation>
                        </tt:Extension>
                    </tt:System>
                    <tt:IO>
                        <tt:InputConnectors>0</tt:InputConnectors>
                        <tt:RelayOutputs>1</tt:RelayOutputs>
                        <tt:Extension>
                            <tt:Auxiliary>false</tt:Auxiliary>
                            <tt:Extension />
                        </tt:Extension>
                    </tt:IO>
                    <tt:Security>
                        <tt:TLS1.1>false</tt:TLS1.1>
                        <tt:TLS1.2>false</tt:TLS1.2>
                        <tt:OnboardKeyGeneration>false</tt:OnboardKeyGeneration>
                        <tt:AccessPolicyConfig>true</tt:AccessPolicyConfig>
                        <tt:X.509Token>false</tt:X.509Token>
                        <tt:SAMLToken>false</tt:SAMLToken>
                        <tt:KerberosToken>false</tt:KerberosToken>
                        <tt:RELToken>false</tt:RELToken>
                        <tt:Extension>
                            <tt:TLS1.0>false</tt:TLS1.0>
                            <tt:Extension>
                                <tt:Dot1X>true</tt:Dot1X>
                                <tt:SupportedEAPMethod>0</tt:SupportedEAPMethod>
                                <tt:RemoteUserHandling>true</tt:RemoteUserHandling>
                            </tt:Extension>
                        </tt:Extension>
                    </tt:Security>
                </tt:Device>
                <tt:Events>
                    <tt:XAddr>http://{host_ip}:{host_port}/onvif/event_service</tt:XAddr>
                    <tt:WSSubscriptionPolicySupport>true</tt:WSSubscriptionPolicySupport>
                    <tt:WSPullPointSupport>true</tt:WSPullPointSupport>
                    <tt:WSPausableSubscriptionManagerInterfaceSupport>false</tt:WSPausableSubscriptionManagerInterfaceSupport>
                </tt:Events>
                <tt:Imaging>
                    <tt:XAddr>http://{host_ip}:{host_port}/onvif/image_service</tt:XAddr>
                </tt:Imaging>
                <tt:Media>
                    <tt:XAddr>http://{host_ip}:{host_port}/onvif/media_service</tt:XAddr>
                    <tt:StreamingCapabilities>
                        <tt:RTPMulticast>false</tt:RTPMulticast>
                        <tt:RTP_TCP>true</tt:RTP_TCP>
                        <tt:RTP_RTSP_TCP>true</tt:RTP_RTSP_TCP>
                    </tt:StreamingCapabilities>
                    <tt:Extension>
                        <tt:ProfileCapabilities>
                            <tt:MaximumNumberOfProfiles>10</tt:MaximumNumberOfProfiles>
                        </tt:ProfileCapabilities>
                    </tt:Extension>
                </tt:Media>
                <tt:PTZ>
                    <tt:XAddr>http://{host_ip}:{host_port}/onvif/ptz_service</tt:XAddr>
                </tt:PTZ>
                <tt:Extension>
                    <tt:DeviceIO>
                        <tt:XAddr>http://{host_ip}:{host_port}/onvif/deviceIO_service</tt:XAddr>
                        <tt:VideoSources>2</tt:VideoSources>
                        <tt:VideoOutputs>0</tt:VideoOutputs>
                        <tt:AudioSources>1</tt:AudioSources>
                        <tt:AudioOutputs>1</tt:AudioOutputs>
                        <tt:RelayOutputs>1</tt:RelayOutputs>
                    </tt:DeviceIO>
                    <tt:Recording>
                        <tt:XAddr>http://{host_ip}:{host_port}/onvif/recording_service</tt:XAddr>
                        <tt:ReceiverSource>false</tt:ReceiverSource>
                        <tt:MediaProfileSource>true</tt:MediaProfileSource>
                        <tt:DynamicRecordings>true</tt:DynamicRecordings>
                        <tt:DynamicTracks>true</tt:DynamicTracks>
                        <tt:MaxStringLength>256</tt:MaxStringLength>
                    </tt:Recording>
                    <tt:Search>
                        <tt:XAddr>http://{host_ip}:{host_port}/onvif/search_service</tt:XAddr>
                        <tt:MetadataSearch>false</tt:MetadataSearch>
                    </tt:Search>
                    <tt:Replay>
                        <tt:XAddr>http://{host_ip}:{host_port}/onvif/replay_service</tt:XAddr>
                    </tt:Replay>
                    <tt:Receiver>
                        <tt:XAddr>http://{host_ip}:{host_port}/onvif/receiver_service</tt:XAddr>
                        <tt:RTP_Multicast>true</tt:RTP_Multicast>
                        <tt:RTP_TCP>true</tt:RTP_TCP>
                        <tt:RTP_RTSP_TCP>true</tt:RTP_RTSP_TCP>
                        <tt:SupportedReceivers>10</tt:SupportedReceivers>
                        <tt:MaximumRTSPURILength>256</tt:MaximumRTSPURILength>
                    </tt:Receiver>
                </tt:Extension>
            </tds:Capabilities>
            </tds:GetCapabilitiesResponse>
        </s:Body>
        </s:Envelope>'''
    return body.encode('utf-8')


def resp_get_scopes():
    body = f'''<?xml version="1.0" encoding="UTF-8"?>
        <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope" 
        xmlns:e="http://www.w3.org/2003/05/soap-encoding" 
        xmlns:wsa="http://www.w3.org/2005/08/addressing" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl" 
        xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2" 
        xmlns:wstop="http://docs.oasis-open.org/wsn/t-1" 
        xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2" 
        xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2" 
        xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2" 
        xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2" 
        xmlns:wsdl="http://schemas.xmlsoap.org/wsdl" 
        xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12" 
        xmlns:http="http://schemas.xmlsoap.org/wsdl/http" 
        xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery" 
        xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing" 
        xmlns:tt="http://www.onvif.org/ver10/schema" 
        xmlns:tns1="http://www.onvif.org/ver10/topics" 
        xmlns:tds="http://www.onvif.org/ver10/device/wsdl" 
        xmlns:trt="http://www.onvif.org/ver10/media/wsdl" 
        xmlns:tev="http://www.onvif.org/ver10/events/wsdl" 
        xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl" 
        xmlns:tst="http://www.onvif.org/ver10/storage/wsdl" 
        xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
        xmlns:tr2="http://www.onvif.org/ver20/media/wsdl" 
        xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl" 
        xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl" 
        xmlns:axt="http://www.onvif.org/ver20/analytics" 
        xmlns:trp="http://www.onvif.org/ver10/replay/wsdl" 
        xmlns:tse="http://www.onvif.org/ver10/search/wsdl" 
        xmlns:trc="http://www.onvif.org/ver10/recording/wsdl" 
        xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl" 
        xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl" 
        xmlns:pt="http://www.onvif.org/ver10/pacs" 
        xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl" 
        xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl" 
        xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl" 
        xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl" 
        xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl" 
        xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl" 
        xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl" 
        xmlns:ter="http://www.onvif.org/ver10/error">
        <s:Header></s:Header>
        <s:Body>
            <tds:GetScopesResponse>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/Profile/Streaming</tt:ScopeItem>
            </tds:Scopes>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/location/country/russia</tt:ScopeItem>
            </tds:Scopes>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/type/image_encoder</tt:ScopeItem>
            </tds:Scopes>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/name/{data_json['device']}</tt:ScopeItem>
            </tds:Scopes>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/hardware/6045</tt:ScopeItem>
            </tds:Scopes>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/Profile/T</tt:ScopeItem>
            </tds:Scopes>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/Profile/G</tt:ScopeItem>
            </tds:Scopes>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/Profile/C</tt:ScopeItem>
            </tds:Scopes>
            <tds:Scopes>
                <tt:ScopeDef>Fixed</tt:ScopeDef>
                <tt:ScopeItem>onvif://www.onvif.org/Profile/A</tt:ScopeItem>
            </tds:Scopes>
            </tds:GetScopesResponse>
        </s:Body>
        </s:Envelope>'''
    return body.encode('utf-8')


def resp_get_dev_info():
    print('Vendor:', data_json['manufacturer'])
    print(data_json['model'])
    print('Version:', data_json['firmware_version'])
    print('S/N:', data_json['serial_number'])
    print(data_json['hardware_id'])
    body = f'''<?xml version="1.0" encoding="UTF-8"?>
        <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope" 
        xmlns:e="http://www.w3.org/2003/05/soap-encoding" 
        xmlns:wsa="http://www.w3.org/2005/08/addressing" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl" 
        xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2" 
        xmlns:wstop="http://docs.oasis-open.org/wsn/t-1" 
        xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2" 
        xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2" 
        xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2" 
        xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2" 
        xmlns:wsdl="http://schemas.xmlsoap.org/wsdl" 
        xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12" 
        xmlns:http="http://schemas.xmlsoap.org/wsdl/http" 
        xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery" 
        xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing" 
        xmlns:tt="http://www.onvif.org/ver10/schema" 
        xmlns:tns1="http://www.onvif.org/ver10/topics" 
        xmlns:tds="http://www.onvif.org/ver10/device/wsdl" 
        xmlns:trt="http://www.onvif.org/ver10/media/wsdl" 
        xmlns:tev="http://www.onvif.org/ver10/events/wsdl" 
        xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl" 
        xmlns:tst="http://www.onvif.org/ver10/storage/wsdl" 
        xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
        xmlns:tr2="http://www.onvif.org/ver20/media/wsdl" 
        xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl" 
        xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl" 
        xmlns:axt="http://www.onvif.org/ver20/analytics" 
        xmlns:trp="http://www.onvif.org/ver10/replay/wsdl" 
        xmlns:tse="http://www.onvif.org/ver10/search/wsdl" 
        xmlns:trc="http://www.onvif.org/ver10/recording/wsdl" 
        xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl" 
        xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl" 
        xmlns:pt="http://www.onvif.org/ver10/pacs" 
        xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl" 
        xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl" 
        xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl" 
        xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl" 
        xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl" 
        xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl" 
        xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl" 
        xmlns:ter="http://www.onvif.org/ver10/error">
        <s:Header></s:Header>
        <s:Body>
            <tds:GetDeviceInformationResponse>
            <tds:Manufacturer>{data_json['manufacturer']}</tds:Manufacturer>
            <tds:Model>{data_json['model']}</tds:Model>
            <tds:FirmwareVersion>{data_json['firmware_version']}</tds:FirmwareVersion>
            <tds:SerialNumber>{data_json['serial_number']}</tds:SerialNumber>
            <tds:HardwareId>{data_json['hardware_id']}</tds:HardwareId>
            </tds:GetDeviceInformationResponse>
        </s:Body>
        </s:Envelope>'''
    return body.encode('utf-8')


def resp_get_serv_cap():
    body = '''<?xml version="1.0" encoding="UTF-8"?>
                <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
                xmlns:e="http://www.w3.org/2003/05/soap-encoding"
                xmlns:wsa="http://www.w3.org/2005/08/addressing"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
                xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
                xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
                xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
                xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
                xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
                xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
                xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
                xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
                xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
                xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
                xmlns:tt="http://www.onvif.org/ver10/schema"
                xmlns:tns1="http://www.onvif.org/ver10/topics"
                xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
                xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
                xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
                xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
                xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
                xmlns:dn="http://www.onvif.org/ver10/network/wsdl"
                xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
                xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
                xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
                xmlns:axt="http://www.onvif.org/ver20/analytics"
                xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
                xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
                xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
                xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
                xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
                xmlns:pt="http://www.onvif.org/ver10/pacs"
                xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
                xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
                xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
                xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
                xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
                xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
                xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
                xmlns:ter="http://www.onvif.org/ver10/error">
                    <s:Header>
                        <wsa:MessageID>uuid:9da8d3e6-a74e-4462-891a-cb8fff0443d2</wsa:MessageID>
                        <wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
                        <wsa:Action>http://www.onvif.org/ver10/events/wsdl/EventPortType/GetServiceCapabilitiesResponse</wsa:Action>
                    </s:Header>
                    <s:Body>
                        <tev:GetServiceCapabilitiesResponse>
                            <tev:Capabilities WSSubscriptionPolicySupport="true" WSPullPointSupport="true" WSPausableSubscriptionManagerInterfaceSupport="false" MaxNotificationProducers="10" MaxPullPoints="10" PersistentNotificationStorage="false">
                            </tev:Capabilities>
                        </tev:GetServiceCapabilitiesResponse>
                    </s:Body>
                </s:Envelope>'''
    return body.encode('utf-8')
