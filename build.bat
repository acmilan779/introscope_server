SET location=C:\Users\n.kruchkov\Desktop\Output\Introscopy\SmartScanServer

pyinstaller --workpath %location% ^
		--distpath %location%\build ^
		--clean ^
		-i %location%\resources\smartscan.ico ^
		%location%\server_auth.py

cd %location%

rmdir /s /q server_auth

move build\server_auth %location%
rmdir /s /q build
rename server_auth build

mkdir build\templates
copy templates build\templates
copy templates build

copy resources\Configs_and_bats build

pause