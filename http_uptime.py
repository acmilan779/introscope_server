import datetime
import json
from uptime import uptime

from flask import Flask, render_template, request

'''Меням в конфиге длительность таймаута для нажатия
оператором на кнопку тревоги'''
def edit_config(text):
    with open('C:/Introscopy_server/dist/config.json', 'r') as file:
        file_d = file.read()
    data = json.loads(file_d)
    data['wait_time'] = text
    with open('C:/Introscopy_server/dist/config.json', 'w') as file:
        json.dump(data, file, indent=2)


# '''Изменяем основные параметры в конфиге на значения
# из формы со страницы /config'''
# def edit_config_admin(input_dict=None):
#     if input_dict is None:
#         input_dict = {}
#     with open('C:/Introscopy_server/dist/config.json', 'r') as file:
#         file_d = file.read()
#     data = json.loads(file_d)
#     for key, value in input_dict.items():
#         if value != '':
#             data[key] = value
#     with open('C:/Introscopy_server/dist/config.json', 'w') as file:
#         json.dump(data, file, indent=2)


'''Получаем время включения компьютера'''
def get_uptime():
    current = datetime.datetime.now().timestamp()
    date_of_boot = datetime.datetime.fromtimestamp(current - uptime()).strftime('%Y-%m-%d %H:%M:%S')
    print(datetime.datetime.fromtimestamp(uptime()).strftime('%Y-%m-%d %H:%M:%S'))
    return date_of_boot


def start_http():

    def read_config():
        with open('C:/Introscopy_server/dist/config.json', 'r') as file:
            file_d = file.read()
        data = json.loads(file_d)
        host_ip = data['ip_adress']
        host_http_port = int(data['http_port'])
        model = data['model']
        serial_number = data['serial_number']
        firmware_ver = data['firmware_version']
        wait_time = data['wait_time']
        device_type = data['device']

    with open('C:/Introscopy_server/dist/config.json', 'r') as file:
        file_d = file.read()
    data = json.loads(file_d)
    host_ip = data['ip_adress']
    host_http_port = int(data['http_port'])
    model = data['model']
    serial_number = data['serial_number']
    firmware_ver = data['firmware_version']
    wait_time = data['wait_time']
    device_type = data['device']

    app = Flask(__name__, template_folder='templates')
    app.config['EXPLAIN_TEMPLATE_LOADING'] = True

    '''Основная страница сервера'''
    @app.route("/")
    def home():
        duration = str(datetime.timedelta(seconds=uptime()))[:-7].split(" ")
        days = '0'
        # read_config()
        if len(duration) == 1:
            time = duration[0]
        else:
            days = duration[0]
            time = duration[2]
        return render_template('C:/Introscopy_server/dist/http_server.html',
                               model=model,
                               serial=serial_number,
                               firmware=firmware_ver,
                               boot_time=get_uptime(),
                               days=days,
                               time=time,
                               wait_time=wait_time)

    '''Выполняется при нажатии кнопки "Сохранить"'''
    @app.route('/', methods=['POST'])
    def wait_time_post():
        text = request.form['text']
        if text != '':
            edit_config(text)
            _wait_time = text
        else:
            _wait_time = wait_time
        duration = str(datetime.timedelta(seconds=uptime()))[:-7].split(" ")
        days = '0'
        if len(duration) == 1:
            time = duration[0]
        else:
            days = duration[0]
            time = duration[2]
        return render_template('C:/Introscopy_server/dist/http_server.html',
                               model=model,
                               serial=serial_number,
                               firmware=firmware_ver,
                               boot_time=get_uptime(),
                               days=days,
                               time=time,
                               wait_time=_wait_time)

    '''Внесение изменений в конфиг-файл'''
    @app.route("/config")
    def config():
        duration = str(datetime.timedelta(seconds=uptime()))[:-7].split(" ")
        # if len(duration) == 1:
        #     is_days = False
        #     time = duration[0]
        # else:
        #     is_days = True
        #     days = duration[0]
        #     time = duration[2]
        return render_template('C:/Introscopy_server/dist/http_server_config.html',
                               host_ip=host_ip,
                               host_port=host_http_port,
                               model=model,
                               firmware=firmware_ver,
                               serial=serial_number,
                               wait_time=wait_time
                               )

    '''Выполняется при нажатии кнопки "Сохранить настройки"'''
    @app.route('/config', methods=['POST'])
    def update_config_post():
        '''Обработка параметров для записи в конфиг'''
        '''Изменяем основные параметры в конфиге на значения
            из формы со страницы /config'''
        if request.form['master_pass'] == '1991':
            if request.form['host_ip'] != '':
                data['ip_adress'] = request.form['host_ip'].replace(',', '.')

            if request.form['http_port'] != '':
                data['http_port'] = request.form['http_port']

            if request.form['model'] != '':
                data['model'] = request.form['model']

            if request.form['firmware'] != '':
                data['firmware_version'] = request.form['firmware']

            if request.form['serial'] != '':
                data['serial_number'] = request.form['serial']

            if request.form['wait_time'] != '':
                data['wait_time'] = request.form['wait_time']

            with open('C:/Introscopy_server/dist/config.json', 'w') as file:
                json.dump(data, file, indent=2)

        '''Выводим сообщение'''
        duration = str(datetime.timedelta(seconds=uptime()))[:-7].split(" ")
        days = '0'
        if len(duration) == 1:
            time = duration[0]
        else:
            days = duration[0]
            time = duration[2]

        return render_template('C:/Introscopy_server/dist/http_server.html',
                               model=model,
                               serial=serial_number,
                               firmware=firmware_ver,
                               boot_time=get_uptime(),
                               days=days,
                               time=time,
                               wait_time=wait_time)

    app.run(host=host_ip, port=host_http_port)


if __name__ == "__main__":
    # start_http('169.254.199.129', 80)
    start_http('', 80)
