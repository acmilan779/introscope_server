# define installer name
OutFile "SmartScanServer.exe"
 
# set desktop as install directory
InstallDir C:\Introscopy_server\dist
 
# default section start
Section
 
# save old files in "C:\Introscopy_server\server_auth_old\" directory
SetOutPath C:\Introscopy_server\

Rename dist\ dist_old\
 
# define output path
SetOutPath $INSTDIR

# specify file to go in output path

; File /r "dist\server_auth\"
File /r "build\"

# SetOutPath C:\Introscopy_server\dist\

File build\config.json
File build\config_intro.json
File build\config_trio.json

File build\registry_add_operator.bat
File build\run_C.bat
File build\run_D.bat
File build\Readme.txt
File /r "build\templates"

# SetOutPath $INSTDIR\resources
# File /r "dist\server_auth\resources\"

# define uninstaller name
WriteUninstaller $INSTDIR\uninstall.exe

# create a popup box, with an OK button and the text 
MessageBox MB_OK "            SmartScan Server$\n      was successfully installed!"
 
#-------
# default section end
SectionEnd

 
# create a section to define what the uninstaller does.
# the section will always be named "Uninstall"
Section "Uninstall"
 
# save old files in "C:\Introscopy_server\server_auth_old\" directory
SetOutPath C:\Introscopy_server\

Rename dist\config.json config.json
 
# define output path
SetOutPath $INSTDIR

# Always delete uninstaller first
Delete $INSTDIR\uninstall.exe

# now delete installed files
Delete $INSTDIR\server_auth.exe
 
Delete $INSTDIR\Readme.txt

RMDir /r $INSTDIR\
 
# Delete the directory
RMDir $INSTDIR

# create a popup box, with an OK button and the text 
MessageBox MB_OK "Deleted!"

SectionEnd