'''Запрос клиента:'''

POST /onvif/event_service HTTP/1.1
Accept: application/soap+xml, text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2
Content-Type: application/soap+xml; charset=utf-8;  action="http://www.onvif.org/ver10/events/wsdl/EventPortType/GetServiceCapabilitiesRequest"
SOAPAction: http://www.onvif.org/ver10/events/wsdl/EventPortType/GetServiceCapabilitiesRequest
Cache-Control: no-cache
Pragma: no-cache
User-Agent: Java/1.8.0_151
Host: 192.168.111.2:8000
Connection: keep-alive
Content-Length: 805

<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
xmlns:a="http://www.w3.org/2005/08/addressing"
xmlns:s="http://www.w3.org/2003/05/soap-envelope">
<env:Header>
<a:Action s:mustUnderstand="1">http://www.onvif.org/ver10/events/wsdl/EventPortType/GetServiceCapabilitiesRequest</a:Action>
<a:MessageID>uuid:9da8d3e6-a74e-4462-891a-cb8fff0443d2</a:MessageID>
<a:To s:mustUnderstand="1">http://192.168.111.2:8000/onvif/event_service</a:To>
<a:ReplyTo>
<a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
</a:ReplyTo>
</env:Header>
<env:Body xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<GetServiceCapabilities xmlns="http://www.onvif.org/ver10/events/wsdl"/>
</env:Body>
</env:Envelope>

HTTP/1.1 200 OK
Server: hsoap/2.8
Content-Type: application/soap+xml; charset=utf-8;  action="http://www.onvif.org/ver10/events/wsdl/EventPortType/GetServiceCapabilitiesRequest"
Content-Length: 2873
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
xmlns:e="http://www.w3.org/2003/05/soap-encoding"
xmlns:wsa="http://www.w3.org/2005/08/addressing"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
xmlns:tt="http://www.onvif.org/ver10/schema"
xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
xmlns:dn="http://www.onvif.org/ver10/network/wsdl"
xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
xmlns:axt="http://www.onvif.org/ver20/analytics"
xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
xmlns:pt="http://www.onvif.org/ver10/pacs"
xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
xmlns:ter="http://www.onvif.org/ver10/error">
<s:Header>
<wsa:MessageID>uuid:9da8d3e6-a74e-4462-891a-cb8fff0443d2</wsa:MessageID>
<wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
<wsa:Action>http://www.onvif.org/ver10/events/wsdl/EventPortType/GetServiceCapabilitiesResponse</wsa:Action>
</s:Header>
<s:Body>
<tev:GetServiceCapabilitiesResponse>
<tev:Capabilities WSSubscriptionPolicySupport="true" WSPullPointSupport="true" WSPausableSubscriptionManagerInterfaceSupport="false" MaxNotificationProducers="10" MaxPullPoints="10" PersistentNotificationStorage="false">
</tev:Capabilities>
</tev:GetServiceCapabilitiesResponse>
</s:Body>
</s:Envelope>

																								from server
																								Subs
POST /onvif/event_service HTTP/1.1
Accept: application/soap+xml, text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2
Content-Type: application/soap+xml; charset=utf-8
Cache-Control: no-cache
Pragma: no-cache
User-Agent: Java/1.8.0_151
Host: 192.168.111.2:8000
Connection: keep-alive
Content-Length: 1049

<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
xmlns:a="http://www.w3.org/2005/08/addressing"
xmlns:s="http://www.w3.org/2003/05/soap-envelope">
<env:Header>
<a:Action s:mustUnderstand="1">http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeRequest</a:Action>
<a:To s:mustUnderstand="1">http://192.168.111.2:8000/onvif/event_service</a:To>
<a:ReplyTo>
<a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
</a:ReplyTo>
</env:Header>
<env:Body xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<ns2:Subscribe xmlns="http://www.w3.org/2005/08/addressing"
xmlns:ns2="http://docs.oasis-open.org/wsn/b-2"
xmlns:ns3="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:ns4="http://docs.oasis-open.org/wsn/t-1">
<ns2:ConsumerReference>
<Address>http://192.168.111.3:80/mq14ydvq8uov86xlmeua</Address>
</ns2:ConsumerReference>
<ns2:InitialTerminationTime>PT3000S</ns2:InitialTerminationTime>
</ns2:Subscribe>
</env:Body>
</env:Envelope>
																							
																							from remote
																							Subs Resp
HTTP/1.1 200 OK
Server: hsoap/2.8
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 2776
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
xmlns:e="http://www.w3.org/2003/05/soap-encoding"
xmlns:wsa="http://www.w3.org/2005/08/addressing"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
xmlns:tt="http://www.onvif.org/ver10/schema"
xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
xmlns:axt="http://www.onvif.org/ver20/analytics"
xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
xmlns:pt="http://www.onvif.org/ver10/pacs"
xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
xmlns:ter="http://www.onvif.org/ver10/error">
<s:Header>
<wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
<wsa:Action>http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeResponse</wsa:Action>
</s:Header>
<s:Body>
<wsnt:SubscribeResponse>
<wsnt:SubscriptionReference>
<wsa:Address>http://192.168.111.2:8000/event_service/0</wsa:Address>
</wsnt:SubscriptionReference>
<wsnt:CurrentTime>2020-02-20T11:22:24Z</wsnt:CurrentTime>
<wsnt:TerminationTime>2020-02-20T11:23:24Z</wsnt:TerminationTime>
</wsnt:SubscribeResponse>
</s:Body>
</s:Envelope>

																							from server
																							Event_1 POST
POST /mq14ydvq8uov86xlmeua HTTP/1.1
Host: 192.168.111.3:80
User-Agent: happytimesoft/1.0
Content-Type: application/soap+xml; charset=utf-8; action="http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify"
Content-Length: 2880
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
xmlns:e="http://www.w3.org/2003/05/soap-encoding"
xmlns:wsa="http://www.w3.org/2005/08/addressing"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
xmlns:tt="http://www.onvif.org/ver10/schema"
xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
xmlns:dn="http://www.onvif.org/ver10/network/wsdl"
xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
xmlns:axt="http://www.onvif.org/ver20/analytics"
xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
xmlns:pt="http://www.onvif.org/ver10/pacs"
xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
xmlns:ter="http://www.onvif.org/ver10/error">
<s:Header>
<wsa:Action>http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify</wsa:Action>
</s:Header>
<s:Body>
<wsnt:Notify>
<wsnt:NotificationMessage>
<wsnt:Topic Dialect="http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet">
tns1:Device/Trigger/DigitalInput
</wsnt:Topic>
<wsnt:Message>
<tt:Message UtcTime="2020-02-20T11:22:24Z" PropertyOperation="Initialized">
<tt:Source>
<tt:SimpleItem Name="InputToken" Value="DIGIT_INPUT_000" />
</tt:Source>
<tt:Data>
<tt:SimpleItem Name="LogicalState" Value="true" />
</tt:Data>
</tt:Message>
</wsnt:Message>
</wsnt:NotificationMessage>
</wsnt:Notify>
</s:Body>
</s:Envelope>

																							from server
																							Event_2 POST
POST /mq14ydvq8uov86xlmeua HTTP/1.1
Host: 192.168.111.3:80
User-Agent: happytimesoft/1.0
Content-Type: application/soap+xml; charset=utf-8; action="http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify"
Content-Length: 2878
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
xmlns:e="http://www.w3.org/2003/05/soap-encoding"
xmlns:wsa="http://www.w3.org/2005/08/addressing"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
xmlns:tt="http://www.onvif.org/ver10/schema"
xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
xmlns:dn="http://www.onvif.org/ver10/network/wsdl"
xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
xmlns:axt="http://www.onvif.org/ver20/analytics"
xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
xmlns:pt="http://www.onvif.org/ver10/pacs"
xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
xmlns:ter="http://www.onvif.org/ver10/error">
<s:Header>
<wsa:Action>http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify</wsa:Action>
</s:Header>
<s:Body>
<wsnt:Notify>
<wsnt:NotificationMessage>
<wsnt:Topic Dialect="http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet">tns1:Device/Trigger/Relay</wsnt:Topic>
<wsnt:Message>
<tt:Message UtcTime="2020-02-20T11:22:24Z" PropertyOperation="Initialized">
<tt:Source>
<tt:SimpleItem Name="RelayToken" Value="RELAY_OUTPUT_000" />
</tt:Source>
<tt:Data>
<tt:SimpleItem Name="LogicalState" Value="inactive" />
</tt:Data>
</tt:Message>
</wsnt:Message>
</wsnt:NotificationMessage>
</wsnt:Notify>
</s:Body>
</s:Envelope>

																							from remote
																							Event_1 POST Resp
HTTP/1.1 200 OK
Connection: close
Date: Thu, 20 Feb 2020 11:19:05 GMT
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 105
Server: Jetty(9.4.8.v20171121)

<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
<env:Header/>
<env:Body/>
</env:Envelope>
																							from remote
																							Event_2 POST Resp
HTTP/1.1 200 OK
Connection: close
Date: Thu, 20 Feb 2020 11:19:05 GMT
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 105
Server: Jetty(9.4.8.v20171121)

<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope">
<env:Header/>
<env:Body/>
</env:Envelope>

																									from remote
																									Subs Renew
POST /event_service/0 HTTP/1.1
Accept: application/soap+xml, text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2
Content-Type: application/soap+xml; charset=utf-8
Cache-Control: no-cache
Pragma: no-cache
User-Agent: Java/1.8.0_151
Host: 192.168.111.2:8000
Connection: keep-alive
Content-Length: 960

<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
xmlns:a="http://www.w3.org/2005/08/addressing"
xmlns:s="http://www.w3.org/2003/05/soap-envelope">
<env:Header>
<a:Action s:mustUnderstand="1">http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/RenewRequest</a:Action>
<a:MessageID>uuid:499d3a20-ce99-40fc-9454-1b3a8e0eeef2</a:MessageID>
<a:To s:mustUnderstand="1">http://192.168.111.2:8000/event_service/0</a:To>
<a:ReplyTo>
<a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
</a:ReplyTo>
</env:Header>
<env:Body xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<Renew xmlns="http://docs.oasis-open.org/wsn/b-2"
xmlns:ns2="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:ns3="http://www.w3.org/2005/08/addressing"
xmlns:ns4="http://docs.oasis-open.org/wsn/t-1">
<TerminationTime>PT3000S</TerminationTime>
</Renew>
</env:Body>
</env:Envelope>

																								from server
																								Subs Renew Resp		
HTTP/1.1 200 OK
Server: hsoap/2.8
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 2710
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
xmlns:e="http://www.w3.org/2003/05/soap-encoding"
xmlns:wsa="http://www.w3.org/2005/08/addressing"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
xmlns:tt="http://www.onvif.org/ver10/schema"
xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
xmlns:axt="http://www.onvif.org/ver20/analytics"
xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
xmlns:pt="http://www.onvif.org/ver10/pacs"
xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
xmlns:ter="http://www.onvif.org/ver10/error">
<s:Header>
<wsa:MessageID>uuid:499d3a20-ce99-40fc-9454-1b3a8e0eeef2</wsa:MessageID>
<wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
<wsa:Action>http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/RenewResponse</wsa:Action>
</s:Header>
<s:Body>
<wsnt:RenewResponse>
<wsnt:TerminationTime>2020-02-20T11:23:25Z</wsnt:TerminationTime>
<wsnt:CurrentTime>2020-02-20T11:22:25Z</wsnt:CurrentTime>
</wsnt:RenewResponse>
</s:Body>
</s:Envelope>

																									from remote
																									Unsubs
POST /event_service/0 HTTP/1.1
Accept: application/soap+xml, text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2
Content-Type: application/soap+xml; charset=utf-8
Cache-Control: no-cache
Pragma: no-cache
User-Agent: Java/1.8.0_151
Host: 192.168.111.2:8000
Connection: keep-alive
Content-Length: 923

<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
xmlns:a="http://www.w3.org/2005/08/addressing"
xmlns:s="http://www.w3.org/2003/05/soap-envelope">
<env:Header>
<a:Action s:mustUnderstand="1">http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/UnsubscribeRequest</a:Action>
<a:MessageID>uuid:c93534b8-135b-4242-bfbd-aff75a10631b</a:MessageID>
<a:To s:mustUnderstand="1">http://192.168.111.2:8000/event_service/0</a:To>
<a:ReplyTo>
<a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
</a:ReplyTo>
</env:Header>
<env:Body xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<Unsubscribe xmlns="http://docs.oasis-open.org/wsn/b-2"
xmlns:ns2="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:ns3="http://www.w3.org/2005/08/addressing"
xmlns:ns4="http://docs.oasis-open.org/wsn/t-1"/>
</env:Body>
</env:Envelope>

																									from server
																									Unsubs Resp
HTTP/1.1 200 OK
Server: hsoap/2.8
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 2606
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
xmlns:e="http://www.w3.org/2003/05/soap-encoding"
xmlns:wsa="http://www.w3.org/2005/08/addressing"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
xmlns:tt="http://www.onvif.org/ver10/schema"
xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
xmlns:axt="http://www.onvif.org/ver20/analytics"
xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
xmlns:pt="http://www.onvif.org/ver10/pacs"
xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
xmlns:ter="http://www.onvif.org/ver10/error">
<s:Header>
<wsa:MessageID>uuid:c93534b8-135b-4242-bfbd-aff75a10631b</wsa:MessageID>
<wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
<wsa:Action>http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/UnsubscribeResponse</wsa:Action>
</s:Header>
<s:Body>
<wsnt:UnsubscribeResponse>
</wsnt:UnsubscribeResponse>
</s:Body>
</s:Envelope>

																									from remote
																									Subs_again
POST /onvif/event_service HTTP/1.1
Accept: application/soap+xml, text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2
Content-Type: application/soap+xml; charset=utf-8
Cache-Control: no-cache
Pragma: no-cache
User-Agent: Java/1.8.0_151
Host: 192.168.111.2:8000
Connection: keep-alive
Content-Length: 1263

<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
xmlns:a="http://www.w3.org/2005/08/addressing"
xmlns:s="http://www.w3.org/2003/05/soap-envelope">
<env:Header>
<a:Action s:mustUnderstand="1">http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeRequest</a:Action>
<a:To s:mustUnderstand="1">http://192.168.111.2:8000/onvif/event_service</a:To>
<a:ReplyTo>
<a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
</a:ReplyTo>
</env:Header>
<env:Body xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<ns2:Subscribe xmlns="http://www.w3.org/2005/08/addressing"
xmlns:ns2="http://docs.oasis-open.org/wsn/b-2"
xmlns:ns3="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:ns4="http://docs.oasis-open.org/wsn/t-1">
<ns2:ConsumerReference>
<Address>http://192.168.111.3:80/fxmsroyblkn5c3wpdz00</Address>
</ns2:ConsumerReference>
<ns2:Filter>
<wsnt:TopicExpression xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2" Dialect="http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet">tns1:Device/FireAlarm/Alarm</wsnt:TopicExpression>
</ns2:Filter>
<ns2:InitialTerminationTime>PT3000S</ns2:InitialTerminationTime>
</ns2:Subscribe>
</env:Body>
</env:Envelope>

																								from server
																								Subs_again Resp
HTTP/1.1 200 OK
Server: hsoap/2.8
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 2776
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
xmlns:e="http://www.w3.org/2003/05/soap-encoding"
xmlns:wsa="http://www.w3.org/2005/08/addressing"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
xmlns:tt="http://www.onvif.org/ver10/schema"
xmlns:tns1="http://www.onvif.org/ver10/topics"
xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
xmlns:axt="http://www.onvif.org/ver20/analytics"
xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
xmlns:pt="http://www.onvif.org/ver10/pacs"
xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
xmlns:ter="http://www.onvif.org/ver10/error">
<s:Header>
<wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
<wsa:Action>http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeResponse</wsa:Action>
</s:Header>
<s:Body>
<wsnt:SubscribeResponse>
<wsnt:SubscriptionReference>
<wsa:Address>http://192.168.111.2:8000/event_service/0</wsa:Address>
</wsnt:SubscriptionReference>
<wsnt:CurrentTime>2020-02-20T11:22:25Z</wsnt:CurrentTime>
<wsnt:TerminationTime>2020-02-20T11:23:25Z</wsnt:TerminationTime>
</wsnt:SubscribeResponse>
</s:Body>
</s:Envelope>

																			decoded from server 
																			Renew Resp
HTTP/1.1 200 OK
Server: hsoap/2.8
Content-Type: application/soap+xml; charset=utf-8
Content-Length: 2710
Connection: close

<?xml version="1.0" encoding="UTF-8"?>
<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope" 
xmlns:e="http://www.w3.org/2003/05/soap-encoding" 
xmlns:wsa="http://www.w3.org/2005/08/addressing" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl" 
xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2" 
xmlns:wstop="http://docs.oasis-open.org/wsn/t-1" 
xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2" 
xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2" 
xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2" 
xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2" 
xmlns:wsdl="http://schemas.xmlsoap.org/wsdl" 
xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12" 
xmlns:http="http://schemas.xmlsoap.org/wsdl/http" 
xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery" 
xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing" 
xmlns:tt="http://www.onvif.org/ver10/schema" 
xmlns:tns1="http://www.onvif.org/ver10/topics" 
xmlns:tds="http://www.onvif.org/ver10/device/wsdl" 
xmlns:trt="http://www.onvif.org/ver10/media/wsdl" 
xmlns:tev="http://www.onvif.org/ver10/events/wsdl" 
xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl" 
xmlns:tst="http://www.onvif.org/ver10/storage/wsdl" 
xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
xmlns:tr2="http://www.onvif.org/ver20/media/wsdl" 
xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl" 
xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl" 
xmlns:axt="http://www.onvif.org/ver20/analytics" 
xmlns:trp="http://www.onvif.org/ver10/replay/wsdl" 
xmlns:tse="http://www.onvif.org/ver10/search/wsdl" 
xmlns:trc="http://www.onvif.org/ver10/recording/wsdl" 
xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl" 
xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl" 
xmlns:pt="http://www.onvif.org/ver10/pacs" 
xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl" 
xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl" 
xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl" 
xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl" 
xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl" 
xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl" 
xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl" 
xmlns:ter="http://www.onvif.org/ver10/error">
<s:Header>
<wsa:MessageID>uuid:14349a6b-3e94-4dd5-8ddb-80b1ff6411ff</wsa:MessageID>
<wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
<wsa:Action>http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/RenewResponse</wsa:Action>
</s:Header>
<s:Body>
<wsnt:RenewResponse>
<wsnt:TerminationTime>2020-04-01T10:24:15Z</wsnt:TerminationTime>
<wsnt:CurrentTime>2020-04-01T10:23:15Z</wsnt:CurrentTime>
</wsnt:RenewResponse>
</s:Body>
</s:Envelope>

																						from remote
																						Subs Request
<env:Envelope
xmlns:env="http://www.w3.org/2003/05/soap-envelope"
xmlns:a="http://www.w3.org/2005/08/addressing"
xmlns:s="http://www.w3.org/2003/05/soap-envelope">
	<env:Header>
	    <a:Action
	        s:mustUnderstand="1">
	        http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeRequest
        </a:Action>
	    <a:To
	        s:mustUnderstand="1">
	        http://169.254.199.129:55000/onvif/event_service
        </a:To>
	    <a:ReplyTo>
	        <a:Address>
	            http://www.w3.org/2005/08/addressing/anonymous
            </a:Address>
        </a:ReplyTo>
    </env:Header>
	<env:Body
	    xmlns:tns1="http://www.onvif.org/ver10/topics"
	    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	    <ns2:Subscribe
	        xmlns="http://www.w3.org/2005/08/addressing"
	        xmlns:ns2="http://docs.oasis-open.org/wsn/b-2"
	        xmlns:ns3="http://docs.oasis-open.org/wsrf/bf-2"
	        xmlns:ns4="http://docs.oasis-open.org/wsn/t-1">
	        <ns2:ConsumerReference>
	            <Address>
	                http://169.254.199.159:55000/zqe4fu3h8rgfaabmh26h
                </Address>
	            </ns2:ConsumerReference>
	        <ns2:InitialTerminationTime>
	            PT3000S
            </ns2:InitialTerminationTime>
        </ns2:Subscribe>
    </env:Body>
</env:Envelope>

																			from server
																			notify event Request
eXtensible Markup Language
<env:Envelope
    xmlns:env="http://www.w3.org/2003/05/soap-envelope"
    xmlns:a="http://www.w3.org/2005/08/addressing"
    xmlns:s="http://www.w3.org/2003/05/soap-envelope">
    <env:Header>
        <a:Action
            s:mustUnderstand="1">
            http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeRequest
            </a:Action>
        <a:To
            s:mustUnderstand="1">
            http://169.254.199.129:55000/onvif/event_service
            </a:To>
        <a:ReplyTo>
            <a:Address>
                http://www.w3.org/2005/08/addressing/anonymous
                </a:Address>
            </a:ReplyTo>
        </env:Header>
    <env:Body
        xmlns:tns1="http://www.onvif.org/ver10/topics"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <ns2:Subscribe
            xmlns="http://www.w3.org/2005/08/addressing"
            xmlns:ns2="http://docs.oasis-open.org/wsn/b-2"
            xmlns:ns3="http://docs.oasis-open.org/wsrf/bf-2"
            xmlns:ns4="http://docs.oasis-open.org/wsn/t-1">
            <ns2:ConsumerReference>
                <Address>
                    http://169.254.199.159:5500/6hjq6e4ib38ujhpgbzay
                    </Address>
                </ns2:ConsumerReference>
            <ns2:Filter>
                <wsnt:TopicExpression
                    xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
                    Dialect="http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet">
                    tns1:Device/MetalDetector/Detect
                    </wsnt:TopicExpression>
                </ns2:Filter>
            <ns2:InitialTerminationTime>
                PT3000S
                </ns2:InitialTerminationTime>
            </ns2:Subscribe>

			<wsnt:Notify>
                    <wsnt:NotificationMessage>
                    <wsnt:Topic Dialect="http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet">tns1:Device/Trigger/DigitalInput</wsnt:Topic>
                    <wsnt:Message>
                        <tt:Message UtcTime="{dtime}" PropertyOperation="Initialized">
                            <tt:Source>
                                <tt:SimpleItem Name="Id" Value="1" />
                            </tt:Source>
                            <tt:Data>
                                <tt:SimpleItem Name="Account" Value="admin" />
                                <tt:SimpleItem Name="Picture" Value="AAAAAAAAAAAAAA==" />
                                <tt:SimpleItem Name="Result" Value="Результат" />
                            </tt:Data>
                        </tt:Message>
                    </wsnt:Message>
                    </wsnt:NotificationMessage>
                </wsnt:Notify>

        </env:Body>
    </env:Envelope>
