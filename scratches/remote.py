#   @ remote program
#   @ каждый вызов encode() и decode() без указания аргумента
#   @ использует кодировку utf-8

import socket
import base64
import time
import struct


def client_init():
    port0 = 9000
    addr0 = '10.101.63.35'
    # addr0 = '127.0.0.1'
    sock = socket.socket()
    sock.connect((addr0, port0))
    print('connected on', addr0, 'port', port0)
    return sock


# def xml_format():
#     e = lxml.builder.ElementMaker()
#     root = e.Message('YYYY-MM-DD', name='UtcTime')
#     the_doc = root
#     source = e.Source
#     key = e.Key
#     datas = e.Data
#     item = e.SimpleItem
#
#     the_doc.append(root(
#         source(
#             item('1', name='Id')
#         ),
#         key(),
#         datas(
#             item('admin', name='Account'),
#             item('aaaa.sxr', name='Picture'),
#             item('Result', name='Result')
#         )
#     ))
#     with open('xml_log.txt', 'wb') as file:
#         file.write(lxml.etree.tostring(the_doc, pretty_print=True))
#     print(lxml.etree.tostring(the_doc, pretty_print=True))


'''
Класс читает пришедшее сообщение, выделяет префикс с размером файла в msglen,
считывает этот файл и возвращает его при вызове recv_msg()
'''


class Recv_data:
    def __init__(self, sock):
        self.sock = sock

    def recieve(self, leng):
        data = bytearray()
        while len(data) < leng:
            packet = sock.recv(leng - len(data))
            if not packet:
                return None
            data.extend(packet)
        return data

    def recv_msg(self):
        raw_msglen = self.recieve(4)
        if not raw_msglen:
            return None
        msglen = struct.unpack('>I', raw_msglen)[0]
        print('msglen = ', msglen)
        return self.recieve(msglen)


'''
Main Process
объявление переменных и инициализация сокета
'''
sock = client_init()
Recv = Recv_data(sock)
log_name = 'DefaultLog.txt'
name = Recv.recv_msg().decode('utf-8')
if 'UserActionsLog' in name:
    log_name = name
print('Log file ', log_name)

try:
    while 1:
        sock.settimeout(None)
        data = sock.recv(1024)
        data = data.decode('utf-8')
        '''
        если пришло сообщение про лог, то считывается путь к нему
        и лог копируется в файл на remote
        '''
        if data == 'New log found!':
            print('new log found ', data)
            f = open(log_name, 'a')
            sock.settimeout(10.0)
            log_buff = []
            try:
                while 1:
                    data = sock.recv(1024).decode('utf-8')
                    if 'New log found' not in data:
                        log_buff.append(data)
                        print('recieved ', data)
            except socket.error:
                print('Logged')
            finally:
                f.write(''.join(log_buff))
                f.close()
                print('Ended')
        '''
        если пришло сообщение про снимок, то считывается путь к нему
        и снимок копируется в одноименный файл на remote
        '''
        if data == 'get sxr':
            print(data)
            data = Recv.recv_msg()
            f = open(data.decode('utf-8'), 'wb')
            sock.settimeout(10.0)
            time.sleep(1.0)
            try:
                encoded_msg = Recv.recv_msg()
                f.write(base64.b64decode(encoded_msg))
            except socket.error:
                print('Transmited')
            finally:
                f.close()
                print('sxr closed')
except ConnectionResetError:
    print('Socket was closed by remote host')
finally:
    sock.close()
