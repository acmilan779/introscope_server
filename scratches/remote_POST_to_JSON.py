import requests

# Making a POST request
# URL = 'https://localhost:80/'
URL = 'https://httpbin.org/post'
r = requests.post(URL, data={'key': 'value'})

# check status code for response recieved
# success code - 200
# print(r)
print(r.status_code)
# responce = r.text
responce = r.json()
headers_list = responce['host']
# print(len(responce['args']))
if r.status_code == 200:
    for ind1 in responce:
        # resp = responce[ind1]
        if isinstance(responce[ind1], dict) and len(responce[ind1]) != 0:
            print(ind1, ': {')
            for ind2 in responce[ind1]:
                print(ind2, '=', responce[ind1][ind2])
            print('}')
        else:
            print(ind1, '=', responce[ind1])
# print('end')
# print(responce)
# print(r.text)
# print(headers_list['Accept'])
print('###Text format###', r.text)

# print content of request

