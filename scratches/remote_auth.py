from requests import Session


class Req:

    def __init__(self,
                 method=None, url=None, headers=None, files=None, data=None,
                 params=None, auth=None, cookies=None, hooks=None, json=None,
                 path_url=None, body=None):
        # Default empty dicts for dict params.
        data = [] if data is None else data
        files = [] if files is None else files
        headers = {} if headers is None else headers
        params = {} if params is None else params
        hooks = {} if hooks is None else hooks

        self.hooks = hooks
        self.method = method
        self.url = url
        self.path_url = path_url
        self.body = body
        self.headers = headers
        self.files = files
        self.data = data
        self.json = json
        self.params = params
        self.auth = auth
        self.cookies = cookies

    def __repr__(self):
        return '<PreparedRequest [%s]>' % self.method

    # def prepare(self):
    #     """Constructs a :class:`PreparedRequest <PreparedRequest>` for transmission and returns it."""
    #     p = Request.PreparedRequest()
    #     p.prepare(
    #         method=self.method,
    #         url=self.url,
    #         path_url=self.path_url,
    #         headers=self.headers,
    #         hooks=self.hooks,
    #         body=self.body
    #         # files=self.files,
    #         # data=self.data,
    #         # json=self.json,
    #         # params=self.params,
    #         # auth=self.auth,
    #         # cookies=self.cookies,
    #     )
    #     return p


def data_split(data_raw):

    def key_list(k_dict, pos):
        k_list = list(k_dict.keys())
        element = [k_list[pos], k_dict[k_list[pos]]]
        return element

    data = ''
    data_structure = ['name', 'age']
    data += '='.join([data_structure[0], key_list(data_raw, 0)[0]])
    data += '&'
    data += '='.join([data_structure[1], key_list(data_raw, 0)[1]])
    return data


def do_request(URL, method, hooks, body, headers, path, data):
    request = Req(headers=headers, method=method,
                  path_url=path + '?' + data,
                  url=URL + path + '?' + data,
                  body=body, hooks=hooks)
    response = Session().send(request)
    return response


def request_post(URL, headers, path, form, data, hooks=None, body=None):
    # if form == 'html':
    #     headers['Accept'] = 'text/html'
    # else:
    #     if form == 'json':
    #         headers['Accept'] = 'application/json'
    #     else:
    #         print(f'Response format "{form}" is not allowed!')
    #         return
    response = do_request(URL, method='POST', headers=headers, path=path, data=data,
                          hooks=hooks, body=body)

    print('Response code:', response.status_code, response.reason)
    print(response.request.method, response.url)
    print(response.elapsed)
    if form == 'html':
        print('Body:\n', response.text)
    else:
        print(response.json())
    return response


def request_get(URL, headers, path, form, data='', hooks=None, body=None):
    if form == 'html':
        headers['Accept'] = 'text/html'
    else:
        if form == 'json':
            headers['Accept'] = 'application/json'
        else:
            print(f'Response format "{form}" is not allowed!')
            return
    response = do_request(URL, method='GET', headers=headers, path=path, data=data,
                          hooks=hooks, body=body)

    print('Response code:', response.status_code, response.reason)
    if form == 'html':
        print(response.text)
    else:
        print(response.json())
    return response


get_cap = '''<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" 
            xmlns:a="http://www.w3.org/2005/08/addressing" xmlns:s="http://www.w3.org/2003/05/soap-envelope">
                <env:Header/>
                <env:Body xmlns:tns1="http://www.onvif.org/ver10/topics" xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <GetCapabilities xmlns="http://www.onvif.org/ver10/device/wsdl">
                        <Category>All</Category>
                    </GetCapabilities>
                </env:Body>
            </env:Envelope>'''
get_scopes = '''<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" 
                xmlns:a="http://www.w3.org/2005/08/addressing" xmlns:s="http://www.w3.org/2003/05/soap-envelope">
                    <env:Header/>
                    <env:Body xmlns:tns1="http://www.onvif.org/ver10/topics" xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <GetScopes xmlns="http://www.onvif.org/ver10/device/wsdl"/>
                    </env:Body>
                </env:Envelope>'''
get_dev_info = '''<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:a="http://www.w3.org/2005/08/addressing" 
                xmlns:s="http://www.w3.org/2003/05/soap-envelope">
                    <env:Header/>
                    <env:Body xmlns:tns1="http://www.onvif.org/ver10/topics" xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <GetDeviceInformation xmlns="http://www.onvif.org/ver10/device/wsdl"/>
                    </env:Body>
                </env:Envelope>'''
get_serv_cap = '''<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
                xmlns:a="http://www.w3.org/2005/08/addressing"
                xmlns:s="http://www.w3.org/2003/05/soap-envelope">
                <env:Header>
                    <a:Action s:mustUnderstand="1">http://www.onvif.org/ver10/events/wsdl/EventPortType/GetServiceCapabilitiesRequest</a:Action>
                    <a:MessageID>uuid:9da8d3e6-a74e-4462-891a-cb8fff0443d2</a:MessageID>
                    <a:To s:mustUnderstand="1">http://192.168.111.2:8000/onvif/event_service</a:To>
                    <a:ReplyTo>
                    <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
                    </a:ReplyTo>
                </env:Header>
                <env:Body xmlns:tns1="http://www.onvif.org/ver10/topics"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <GetServiceCapabilities xmlns="http://www.onvif.org/ver10/events/wsdl"/>
                </env:Body>
                </env:Envelope>'''
get_subs = '''<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
            xmlns:a="http://www.w3.org/2005/08/addressing"
            xmlns:s="http://www.w3.org/2003/05/soap-envelope">
                <env:Header>
                    <a:Action s:mustUnderstand="1">http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeRequest</a:Action>
                    <a:To s:mustUnderstand="1">http://192.168.111.2:8000/onvif/event_service</a:To>
                    <a:ReplyTo>
                        <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
                    </a:ReplyTo>
                </env:Header>
                <env:Body xmlns:tns1="http://www.onvif.org/ver10/topics"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <ns2:Subscribe xmlns="http://www.w3.org/2005/08/addressing"
                    xmlns:ns2="http://docs.oasis-open.org/wsn/b-2"
                    xmlns:ns3="http://docs.oasis-open.org/wsrf/bf-2"
                    xmlns:ns4="http://docs.oasis-open.org/wsn/t-1">
                        <ns2:ConsumerReference>
                            <Address>http://192.168.111.3:80/mq14ydvq8uov86xlmeua</Address>
                        </ns2:ConsumerReference>
                        <ns2:InitialTerminationTime>PT3000S</ns2:InitialTerminationTime>
                    </ns2:Subscribe>
                </env:Body>
            </env:Envelope>'''
test_get_cap = '<Category>All</Category>'

resp_event = '''<?xml version="1.0" encoding="UTF-8"?>
            <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
            xmlns:e="http://www.w3.org/2003/05/soap-encoding"
            xmlns:wsa="http://www.w3.org/2005/08/addressing"
            xmlns:xs="http://www.w3.org/2001/XMLSchema"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
            xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
            xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
            xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
            xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
            xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
            xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
            xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
            xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
            xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
            xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
            xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
            xmlns:tt="http://www.onvif.org/ver10/schema"
            xmlns:tns1="http://www.onvif.org/ver10/topics"
            xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
            xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
            xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
            xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
            xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
            xmlns:dn="http://www.onvif.org/ver10/network/wsdl"
            xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
            xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
            xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
            xmlns:axt="http://www.onvif.org/ver20/analytics"
            xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
            xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
            xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
            xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
            xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
            xmlns:pt="http://www.onvif.org/ver10/pacs"
            xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
            xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
            xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
            xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
            xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
            xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
            xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
            xmlns:ter="http://www.onvif.org/ver10/error">
                <s:Header>
                    <wsa:Action>http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify</wsa:Action>
                </s:Header>
                <s:Body>
                    <wsnt:Notify>
                        <wsnt:NotificationMessage>
                        <wsnt:Topic Dialect="http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet">tns1:Device/Trigger/DigitalInput</wsnt:Topic>
                        <wsnt:Message>
                            <tt:Message UtcTime="2020-02-20T11:22:24Z" PropertyOperation="Initialized">
                                <tt:Source>
                                    <tt:SimpleItem Name="InputToken" Value="DIGIT_INPUT_000" />
                                </tt:Source>
                                <tt:Data>
                                    <tt:SimpleItem Name="LogicalState" Value="true" />
                                </tt:Data>
                            </tt:Message>
                        </wsnt:Message>
                        </wsnt:NotificationMessage>
                    </wsnt:Notify>
                </s:Body>
            </s:Envelope>'''

# URL = 'http://localhost:55000'
# URL = 'http://169.254.199.129:55000'
# host_ip = '169.254.199.159'
host_ip = 'localhost'
# host_port = input('Remote port')
host_port = 55000
adress = input('Remote adress')
# adress = '/eumkjbqm0b1pk78c0v05'
URL = f'http://{host_ip}:{host_port}'
# headers = {'Host': 'Test'}
path_device = '/onvif/device_service'
path_event = '/onvif/event_service'
# data_raw = {'Steven': '30'}
data_raw = {'Jack': '29'}
data = data_split(data_raw)
# hooks = {'response': []}
# body = '<<  GetCapabilities bodytexthere  >>'
form = 'html'

# request_post(URL, headers, path_device, form, data, body=get_cap)
# request_post(URL, headers, path_device, form, data, body=get_cap)
# request_post(URL, headers, path_device, form, data, body=get_scopes)
# request_post(URL, headers, path_device, form, data, body=get_dev_info)
# #
# request_post(URL, headers, path_event, form, data, body=get_serv_cap)
# request_post(URL, headers, path_event, form, data, body=get_subs)
body = resp_event
# headers = [('Content-Type', 'application/soap+xml; charset=utf-8; action="http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify"'),
#            ('Content-Length', len(body)),
#            ('Host', f'{host_ip}:{host_port}'),
#            ('User-Agent', 'pycharm/1.0'),
#            ('Connection', 'close')]
headers = {'Content-Type': 'application/soap+xml; charset=utf-8; '
                           'action="http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify"',
           'Content-Length': len(body),
           'Host': f'{host_ip}:{host_port}',
           'User-Agent': 'pycharm/1.0',
           'Connection': 'close'}
request_post(URL, headers, path=path_event, form=form, data=data, body=body)

# response = request_get(URL=URL, headers=headers, path=path, form=form)
# '''POST-Request'''
# request = Req(hooks={'response': []}, body='name=Peter&age=25',
#               headers=headers, method='POST',
#               path_url=path + '?' + data,
#               url=URL + path + '?' + data)
# response = Session().send(request)
# print(response.text)
# '''GET-Request'''
# headers['Accept'] = 'text/html'
# headers['Accept'] = 'application/json'
# request = Req(hooks={'response': []}, body='',
#               headers=headers, method='GET',
#               path_url=path + '?' + data,
#               url=URL + path + '?' + data)
# response = Session().send(request)
# print(response.json())
