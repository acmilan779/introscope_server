import serial
import time
import random
import datetime
import serial_port_receiver


def send_rad():
    message = b'\xcc'  # преамбула
    message += b'\x00'  # уровень металла
    message += b'\x03'  # уровень радиации
    message += b'\x00'  # уровень взрывчатки
    message += b'\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00'  # 11-111
    message += b'\x00\x00\x00\x02\x00\x00\x00\x00\x00\x02\x00'  # 21-211
    message += b'\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00'  # 31-311
    message += b'\x00\x00\x00\x00'  # выравниватель до 40 байт
    return message


def send_bomb():
    message = b'\xcc'  # преамбула
    message += b'\x00'  # уровень металла
    message += b'\x00'  # уровень радиации
    message += b'\x0C'  # уровень взрывчатки
    message += b'\x03\x00\x03\x00\x00\x00\x00\x00\x00\x00\x00'  # 11-111
    message += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'  # 21-211
    message += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'  # 31-311
    message += b'\x00\x00\x00\x00'  # выравниватель до 40 байт
    return message


def send_metal():
    message = b'\xcc'  # преамбула
    message += b'\x0A'  # уровень металла
    message += b'\x00'  # уровень радиации
    message += b'\x00'  # уровень взрывчатки
    message += b'\x01\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00'  # 11-111
    message += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'  # 21-211
    message += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01'  # 31-311
    message += b'\x00\x00\x00\x00'  # выравниватель до 40 байт
    return message


ser = serial.Serial()
ser.baudrate = 57600
ser.port = 'COM2'
ser.timeout = 1
ser.parity = 'N'
ser.stopbits = 1
ser.bytesize = 8
ser.open()

# message = b'\xcc'  # преамбула
# message += b'\x01'  # уровень металла
# message += b'\x03'  # уровень радиации
# message += b'\x0C'  # уровень взрывчатки
# # metal_list = [0 for i in range(33)]
# # rad_list = [0 for i in range(33)]
# # rand_count = random.randint(1, 6)
# # for i in range(rand_count):
# #     rand_i = random.randint(1, 33)
# #     rand_val = random.randint(1, 3)
# #     if rand_val == 1:
# #         metal_list[rand_i-1] = rand_val
# #     elif rand_val == 2:
# #         rad_list[rand_i-1] = rand_val
# #     elif rand_val == 3:
# #         metal_list[rand_i-1] = rand_val
# #         rad_list[rand_i-1] = rand_val
#     # message += rand_i.to_bytes(1, 'big')
# message += b'\x01\x02\x01\x00\x00\x00\x00\x00\x00\x00\x00'  # 1-11
# message += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'  # 2-22
# message += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03'  # 3-33
# print(metal_list)
# print(rad_list)
# print(len(message))
x = 1
while x > 0:
    # message = b'\xcc'  # преамбула
    # message += b'\x0A'  # уровень металла
    # # message += b'\x00'  # уровень радиации
    # message += b'\x03'  # уровень радиации
    # message += b'\x0C'  # уровень взрывчатки
    # message += b'\x01\x02\x01\x00\x00\x00\x00\x00\x00\x00\x00'  # 11-111
    # message += b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00'  # 21-211
    # message += b'\x00\x02\x00\x00\x00\x03\x00\x00\x00\x00\x01'  # 31-311
    # message += b'\x00\x00\x00\x00'  # выравниватель до 40 байт

    # ser.write(send_rad())
    # ser.write(send_bomb())
    ser.write(send_metal())

    # ser.write(message)
    # print(message)
    time.sleep(1)
    print(ser.read(2))
    x -= 1
    print(x)

    # print(serial_port_receiver.serve_serial_port())
