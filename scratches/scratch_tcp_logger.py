import tcp_logger
import socket
import time

# host = '169.254.199.129'
# host = '0.0.0.0'
host = '10.101.63.45'
port = 5000

'''TCP Logger Init'''
serv_sock_tcp = socket.socket(
    socket.AF_INET,
    socket.SOCK_STREAM,
    proto=0)
init_key = False
# print(self.dtime())
while init_key is not True:
    error_key = False
    try:
        error_key = False
        serv_sock_tcp.bind((host, 23))
    except OSError as e:
        error_key = True
        print('TCP Logger: ', e.strerror)
        time.sleep(1.0)
    finally:
        if error_key is False:
            init_key = True
print('TCP Logger starting...')

try:
    serv_sock_tcp.listen()
    while True:
        serv_sock_tcp.settimeout(2)

        try:
            # print(serv_sock_tcp.accept())
            tcp_conn = serv_sock_tcp.accept()[0]
            tcp_logger.serve_tcp_logger(tcp_conn, 'Trio')
        except socket.error as e:
            print('TCP Logger: ', e)

finally:
    print('Serving ended')
    serv_sock_tcp.close()

# tcp_logger.serve_tcp_logger(host, port)
