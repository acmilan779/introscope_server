#   @ server program
#   @ каждый вызов encode() и decode() без указания аргумента
#   @ использует кодировку utf-8

import socket
import base64
import time
import struct
import json

import os
import win32con
import win32event
import win32file
from collections import deque
from datetime import datetime

import sxr_decode

'''Инициализация и запуск tcp-сервера на порту 9000
возвращает созданный сокет'''
def tcp_init():
    port = 9000
    sock = socket.socket()
    sock.bind(('10.101.63.45', port))
    sock.listen(1)
    print('Wait for client to connect...')
    conn, addr = sock.accept()
    print('connected:', addr)
    return conn


'''поиск файла с логами 'UserActionsLog_dd.mm.yyyy hh-mm-ss-uuuuuuu.txt'
возвращает полное имя этого файла'''
def subdirs(path):
    for entry in os.scandir(path):
        if entry.name.startswith('UserActionsLog') and entry.is_file():
            return entry.name


# def check_folder():
#

'''main-функция для запросов из body_XMLs'''
def image_main():

    '''Main Process
    объявление переменных'''
    # path_to_logs = os.path.abspath('D:\BagVision\Client\BagScan\logs')
    path_to_archive = os.path.abspath('D:\BagVision\Client\Archive')

    '''Считываем данные времени ожидания из конфига '''
    with open('C:/Introscopy_server/dist/config.json', 'r') as file:
        file_d = file.read()
    data = json.loads(file_d)
    wait_time = int(data['wait_time'])

    '''находим файл UserActionsLog, записываем его название в log_file_name
    и полный путь к нему в logs_path_abs.
    Находим папку с архивом для текущей даты либо создаем ее в try: ... except: ...'''
    # logs_file_name = subdirs(path_to_logs)
    # # print('main^ Log-файл найден: ', logs_file_name)
    # logs_path_abs = path_to_logs + '\\' + logs_file_name
    # Log = LogFile(logs_path_abs)

    now = datetime.now()
    year = now.strftime('%Y')
    month = now.strftime('%m')
    if int(month) < 10:
        month = month[1]
    day = now.strftime('%d')
    if int(day) < 10:
        day = day[1]
    path_to_archive = path_to_archive + '\\' + year
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_year is already exist')
        pass
    path_to_archive = path_to_archive + '\\' + month
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_month is already exist')
        pass
    path_to_archive = path_to_archive + '\\' + day
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_day is already exist')
        pass
    # print('main^ Archive is in', path_to_archive)

    '''подсчет количества строк лог-файла в lines
    запись последней непустой строки в last_log
    запись номера строки с последним логом в log_pos'''
    # lines = Log.log_lenght()
    # last_log = Log.last_log()
    # # print('main^ Last log from init: ', last_log)
    # log_pos = Log.last_log_line(last_log)

    '''включаем отслеживание состояния файлов в папке с логами
    реагирует на время последней перезаписи файлов по ..LAST_WRITE
    и на изменение имен и количества файлов по ..FILE_NAME'''
    # change_handle = win32file.FindFirstChangeNotification(
    #     path_to_logs,
    #     0,
    #     win32con.FILE_NOTIFY_CHANGE_LAST_WRITE)

    change_handle_SXR = win32file.FindFirstChangeNotification(
        path_to_archive,
        0,
        win32con.FILE_NOTIFY_CHANGE_FILE_NAME)

    try:
        old_files = dict([(f, None) for f in os.listdir(path_to_archive)])
        flag = True

        while flag:
            # write_state = win32event.WaitForSingleObject(change_handle, 50)
            adding_state = win32event.WaitForSingleObject(change_handle_SXR, 50)

            '''Check if there are new files in folder'''
            if adding_state == 0:
                flag = False

                new_files = dict([(f, None) for f in os.listdir(path_to_archive)])
                added = [f for f in new_files if f not in old_files]
                deleted = [f for f in old_files if f not in new_files]
                old_files = new_files
                win32file.FindNextChangeNotification(change_handle_SXR)
                if added:
                    if 'Dangerous' in added[0]:
                        time.sleep(wait_time)
                        path_to_new_image = path_to_archive + '\\' + added[0]
                        return path_to_new_image, True
                    else:
                        time.sleep(wait_time)
                        '''For BagScan version 6.6 and higher check metadata'''
                        new_sxr_path = path_to_archive + '\\' + added[0]
                        sxr_object = sxr_decode.SXR(new_sxr_path)
                        ver = sxr_object.check_bagscan_version()[1:-2]
                        ver = ver.decode('utf-8').split('.')
                        if len(ver) >= 4:
                            bagscan_version = ver[0] + ver[1] + '.' + ver[2] + ver[3]
                        else:
                            bagscan_version = 0
                        new_bagscan_flag = False
                        if float(bagscan_version) > 66.0:
                            marked_sxr_path = sxr_object.check_metadata()
                            # print(added[0])
                            # print(marked_sxr_path)
                            added_new = [marked_sxr_path]
                            new_bagscan_flag = True
                        else:
                            new_files = dict([(f, None) for f in os.listdir(path_to_archive)])
                            added_new = [f for f in new_files if f not in old_files]

                    if added_new and 'Dangerous' in added_new[0]:
                        if new_bagscan_flag:
                            path_to_new_image = path_to_archive + '\\' + added[0]
                        else:
                            path_to_new_image = path_to_archive + '\\' + added_new[0]
                        return path_to_new_image, True
                    path_to_new_image = path_to_archive + '\\' + added[0]
                    return path_to_new_image, False

                # elif deleted:
                #     return '0'

                # else:
                #     return '0'

    finally:
        # win32file.FindCloseChangeNotification(change_handle)
        win32file.FindCloseChangeNotification(change_handle_SXR)


'''класс Log содержит функции по поиску последней строки, ее позиции
и количеству свежих логов до их передачи клиенту'''
class LogFile:

    def __init__(self, log_file_path):
        self.log_file_path = log_file_path

    ##  поиск и возврат последней непустой строки лог-файла
    def last_log(self):
        last_log = ''
        flag = True
        n = 1
        while flag:
            with open(self.log_file_path) as file:
                last = list(deque(file,n))  ##  читает и преобразует одну строку с конца файла
                if '\n' == last[0]:
                    n += 1
                else:
                    last_cleaned = last[0].strip()
                    flag = False
        return last_log.join(last_cleaned)

    ##  подсчет количества строк в лог-файле
    def log_lenght(self):
        lines = 0
        with open(self.log_file_path) as file:
            for line in file:
                lines += 1
        # print('LogLength^ line number: ', lines)
        return lines

    ##  принимает последний лог и возвращает номер этой строки
    def last_log_line(self, last_log):
        n = 10
        m = 10
        log_line = 'Error in last_log_line'
        log_len = self.log_lenght()
        # print('last_log_line^ amount of lines: ', log_len)
        if log_len < m:
            m = log_len-1
        flag = True
        while flag:
            for i in range(m, log_len, n):
                if flag:
                    with open(self.log_file_path) as file:
                        last = list(deque(file, i))
                    last = [line.strip() for line in last]
                    if last_log in last:
                        last.reverse()
                        log_line = log_len - last.index(last_log)
                        flag = False
        # last.clear()
        return log_line

    ##  принимает последнюю переданную строку и возращает все строки добавленные после нее
    def read_new_log(self, last_log):
        log_len = self.log_lenght()
        log_line_from_end = log_len - self.last_log_line(last_log)
        # print('read_new_log^ prev pos of log from end: ', log_line_from_end)
        with open(self.log_file_path) as file:
            last = list(deque(file,log_line_from_end+1))
        for line in last:
            if line != '\n':
                last = [line.strip() for line in last]
        if last_log not in last:
            print('Error in read_new_log')
        new_logs = last[1:]
        return new_logs


'''класс Image содержит путь к архиву с текущей датой
и функции по отправке и кодированию свежего снимка'''
class ImageFile:

    def __init__(self, image_folder_path):
        self.image_folder_path = image_folder_path

    ##  добавляет к сообщению префикс с размером сообщения
    ##  пересылает сообщение по указанному сокету
    def send_data(self, conn, msg):
        # print('Msg size = ', len(msg))
        msg = struct.pack('>I', len(msg)) + msg
        conn.send(msg)

    ##  использует send_data, чтобы передать название файла с изображением
    def send_name(self, image_name, conn):
        self.send_data(conn, image_name.encode())
        # conn.send(image_name.encode('utf-8'))
        # print('file name len: ',len(image_name.encode('utf-8')))

    ##  кодирует файл изображения в base64
    ##  и передает его используя send_data
    def send_image(self, image_name, conn):
        file = open(self.image_folder_path + '\\' + image_name, 'rb')
        sxr = base64.b64encode(file.read(70*1024*1024))   # 70MB
        print(len(sxr))
        self.send_data(conn, sxr)
        file.close()


if __name__ == '__main__':
    '''
    Main Process
    объявление переменных и открытие порта
    '''
    path_to_logs = os.path.abspath('D:\BagVision\Client\BagScan\logs')
    path_to_archive = os.path.abspath('D:\BagVision\Client\Archive')
    conn = tcp_init()

    '''находим файл UserActionsLog, записываем его название в log_file_name
    и полный путь к нему в logs_path_abs.
    Находим папку с архивом для текущей даты либо создаем ее в try: ... except: ...'''
    logs_file_name = subdirs(path_to_logs)
    print('main^ Log-файл найден: ', logs_file_name)
    logs_path_abs = path_to_logs + '\\' + logs_file_name
    Log = LogFile(logs_path_abs)

    now = datetime.now()
    year = now.strftime('%Y')
    month = now.strftime('%m')
    if int(month) < 10:
        month = month[1]
    day = now.strftime('%d')
    if int(day) < 10:
        day = day[1]
    path_to_archive = path_to_archive + '\\' + year
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_year is already exist')
        pass
    path_to_archive = path_to_archive + '\\' + month
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_month is already exist')
        pass
    path_to_archive = path_to_archive + '\\' + day
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_day is already exist')
        pass
    Image = ImageFile(path_to_archive)
    print('main^ Archive is in', path_to_archive)

    '''подсчет количества строк лог-файла в lines
    запись последней непустой строки в last_log
    запись номера строки с последним логом в log_pos'''
    lines = Log.log_lenght()
    last_log = Log.last_log()
    print('main^ Last log from init: ', last_log)
    log_pos = Log.last_log_line(last_log)

    '''включаем отслеживание состояния файлов в папке с логами
    реагирует на время последней перезаписи файлов по ..LAST_WRITE
    и на изменение имен и количества файлов по ..FILE_NAME'''
    change_handle = win32file.FindFirstChangeNotification(
        path_to_logs,
        0,
        win32con.FILE_NOTIFY_CHANGE_LAST_WRITE)

    change_handle_SXR = win32file.FindFirstChangeNotification(
        path_to_archive,
        0,
        win32con.FILE_NOTIFY_CHANGE_FILE_NAME)

    try:
        old_files = dict([(f, None) for f in os.listdir(path_to_archive)])
        conn.send(logs_file_name.encode('utf-8'))
        Image.send_name(logs_file_name, conn)

        while 1:
            write_state = win32event.WaitForSingleObject(change_handle, 50)
            adding_state = win32event.WaitForSingleObject(change_handle_SXR, 50)

            ##  при внесении изменений в файл write_state сбрасывается в 0
            if write_state == 0:
                msg = 'New log found!'
                conn.send(msg.encode('utf-8'))
                print(msg)
                # print('Previous log: ', last_log)
                # conn.send(logs_file_name.encode('utf-8'))
                new_logs = Log.read_new_log(last_log)
                print('Logs count: ', len(new_logs))
                for line in new_logs:
                    conn.send((line + '\n').encode('utf-8'))
                print('New log: ', new_logs)
                if len(new_logs) != 0:
                    last_log = Log.last_log()
                print('Current last log: ', last_log)
                win32file.FindNextChangeNotification(change_handle)

            ##  при добавлении/удалении/переименовании файла в папке сбрасывается в 0
            if adding_state == 0:
                new_files = dict([(f, None) for f in os.listdir(path_to_archive)])
                added = [f for f in new_files if f not in old_files]
                deleted = [f for f in old_files if f not in new_files]
                if added:
                    msg = 'get sxr'
                    conn.send(msg.encode('utf-8'))
                    print(msg)
                    Image.send_name(added[0], conn)
                    time.sleep(3.0)
                    Image.send_image(added[0], conn)

                if deleted:
                    print('Deleted:', ', '.join(deleted))

                old_files = new_files
                win32file.FindNextChangeNotification(change_handle_SXR)

    finally:
        win32file.FindCloseChangeNotification(change_handle)
        win32file.FindCloseChangeNotification(change_handle_SXR)
