#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging


class HttpProcessor(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.wfile.write(b"hello !")
        self.send_header('content-type','text/html')
        self.end_headers()
        self.wfile.write(b"hello !")

    def do_POST(self):
        self.send_response(200)
        self.wfile.write(b"hello !")
        self.version_string()


serv = HTTPServer(("localhost", 80), HttpProcessor)
print('Starting...')
serv.serve_forever()

# class S(BaseHTTPRequestHandler):
#     def _set_response(self):
#         self.send_response(200)
#         self.send_header('Content-type', 'text/html')
#         self.end_headers()
#
#     def do_GET(self):
#         print('getting GET')
#         logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.host))
#         self._set_response()
#         self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))
#
#     def do_POST(self):
#         print('getting POST')
#         content_length = int(self.host['Content-Length']) # <--- Gets the size of data
#         post_data = self.rfile.read(content_length) # <--- Gets the data itself
#         logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
#                 str(self.path), str(self.host), post_data.decode('utf-8'))
#
#         self._set_response()
#         self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
#
# def run(server_class=HTTPServer, handler_class=S, port=8080):
#     logging.basicConfig(level=logging.INFO)
#     server_address = ('10.101.63.45', port)
#     httpd = server_class(server_address, handler_class)
#     logging.info('Starting httpd...\n')
#     try:
#         httpd.serve_forever()
#     except KeyboardInterrupt:
#         pass
#     httpd.server_close()
#     logging.info('Stopping httpd...\n')
#
# if __name__ == '__main__':
#     from sys import argv
#
#     if len(argv) == 2:
#         run(port=int(argv[1]))
#     else:
#         run()
#