#
#
# origin_file = open('C:\\Users\\n.kruchkov\\Desktop\\test_1.sxr', 'rb')
# new_file = open('C:\\Users\\n.kruchkov\\Desktop\\test_1_1.bmp', 'wb')
# buff = origin_file.read(70*1024*1024)
# new_file.write(buff)
# new_file.close()
import struct


def convert(line, size):
    line_str = str(line)
    return (int(line)).to_bytes(size, byteorder="big")

def bytes_to_int(in_bytes):
    result = 0
    for b in in_bytes:
        result = result * 256 + int(b)
    return result

with open("bmp_big.bmp","w+b") as f:
    # f.write(b'BM')#ID field (42h, 4Dh)
    # f.write((154).to_bytes(4,byteorder="little"))#154 bytes (122+32) Size of the BMP file
    # f.write((0).to_bytes(2,byteorder="little"))#Unused
    # f.write((0).to_bytes(2,byteorder="little"))#Unused
    # f.write((122).to_bytes(4,byteorder="little"))#122 bytes (14+108) Offset where the pixel array (bitmap data) can be found
    # f.write((108).to_bytes(4,byteorder="little"))#108 bytes Number of bytes in the DIB header (from this point)
    # f.write((4).to_bytes(4,byteorder="little"))#4 pixels (left to right order) Width of the bitmap in pixels
    # f.write((2).to_bytes(4,byteorder="little"))#2 pixels (bottom to top order) Height of the bitmap in pixels
    # f.write((1).to_bytes(2,byteorder="little"))#1 plane Number of color planes being used
    # f.write((32).to_bytes(2,byteorder="little"))#32 bits Number of bits per pixel
    # f.write((3).to_bytes(4,byteorder="little"))#3 BI_BITFIELDS, no pixel array compression used
    # f.write((32).to_bytes(4,byteorder="little"))#32 bytes Size of the raw bitmap data (including padding)
    # f.write((2835).to_bytes(4,byteorder="little"))#2835 pixels/metre horizontal Print resolution of the image,
    # f.write((2835).to_bytes(4,byteorder="little"))#2835 pixels/metre vertical   72 DPI × 39.3701 inches per metre yields 2834.6472
    # f.write((0).to_bytes(4,byteorder="little"))#0 colors Number of colors in the palette
    # f.write((0).to_bytes(4,byteorder="little"))#0 important colors 0 means all colors are important
    # f.write(b'\x00\x00\xFF\x00')#00FF0000 in big-endian Red channel bit mask (valid because BI_BITFIELDS is specified)
    # f.write(b'\x00\xFF\x00\x00')#0000FF00 in big-endian Green channel bit mask (valid because BI_BITFIELDS is specified)
    # f.write(b'\xFF\x00\x00\x00')#000000FF in big-endian Blue channel bit mask (valid because BI_BITFIELDS is specified)
    # f.write(b'\x00\x00\x00\xFF')#FF000000 in big-endian Alpha channel bit mask
    # f.write(b' niW')#little-endian "Win " LCS_WINDOWS_COLOR_SPACE
    # f.write((0).to_bytes(36,byteorder="little"))#CIEXYZTRIPLE Color Space endpoints	Unused for LCS "Win " or "sRGB"
    # f.write((0).to_bytes(4,byteorder="little"))#0 Red Gamma Unused for LCS "Win " or "sRGB"
    # f.write((0).to_bytes(4,byteorder="little"))#0 Green Gamma Unused for LCS "Win " or "sRGB"
    # f.write((0).to_bytes(4,byteorder="little"))#0 Blue Gamma Unused for LCS "Win " or "sRGB"
    # for value in range(0, 0xFF, 32):
    #     line = str(value) + '\x00\x00\xFF'
    #     f.write(line.encode())
    # f.write(b'\xFF\x00\x00\x7F')#255 0 0 127 Blue (Alpha: 127), Pixel (1,0)
    # f.write(b'\x00\xFF\x00\x7F')#0 255 0 127 Green (Alpha: 127), Pixel (1,1)
    # f.write(b'\x00\x00\xFF\x7F')#0 0 255 127 Red (Alpha: 127), Pixel (1,2)
    # f.write(b'\xFF\xFF\xFF\x7F')#255 255 255 127 White (Alpha: 127), Pixel (1,3)
    # f.write(b'\xFF\x00\x00\xFF')#255 0 0 255 Blue (Alpha: 255), Pixel (0,0)
    # f.write(b'\x00\xFF\x00\xFF')#0 255 0 255 Green (Alpha: 255), Pixel (0,1)
    # f.write(b'\x00\x00\xFF\xFF')#0 0 255 255 Red (Alpha: 255), Pixel (0,2)
    # f.write(b'\xFF\xFF\xFF\xFF')#255 255 255 255 White (Alpha: 255), Pixel (0,3)

    # f.write(convert(b'424d'))
    # f.write(convert(b'ae000000'))
    f.write(convert(0x424D, 2)) #BM
    f.write(convert(0x40c27900, 4)) # file size (bytes)
    f.write(convert(0x00000000, 4)) # reserved
    f.write(convert(0x36000000, 4)) # offset of img data
    f.write(convert(0x28000000, 4)) # DIB hdr size
    f.write(convert(0x0f040000, 4)) # width
    f.write(convert(0x80020000, 4)) # height
    f.write(convert(0x01001800, 4)) # #/planes + bits/px
    f.write(convert(0x00000000, 4)) # compress
    f.write(convert(0x78000000, 4)) # img size (bytes)
    f.write(convert(0x00000000, 4)) # x resolution
    f.write(convert(0x00000000, 4)) # y resolution
    f.write(convert(0x00000000, 4)) # #/colors
    f.write(convert(0x00000000, 4)) # #/important colors

    # origin_file = open('C:\\Users\\n.kruchkov\\Desktop\\test_1.sxr', 'rb')
    # sxr_file = origin_file.readline(70)
    # with open('C:\\Users\\n.kruchkov\\Desktop\\test_2.sxr', 'rb') as file:
    #     # sxr_file = [row.strip() for row in file]
    #     sxr_file = file.read(70*1024*1024)
    # print(sxr_file[:-5:-1])
    # print(len(sxr_file))
    # buff = sxr_file
    # for Energy data
    # for byte_pos in range(0, len(sxr_file), 4):
    #     buff = sxr_file[byte_pos:byte_pos+2]
    #     if buff >= b'EA60':
    #         f.write(convert(0x0000ff0000ff, 6)) # red
    #     elif buff >= b'D6D8':
    #         f.write(convert(0x0080ff0080ff, 6))  # orange
    #     elif buff >= b'C350':
    #         f.write(convert(0x00ff0000ff00, 6))  # green
    #     else:
    #         f.write(convert(0xff8000ff8000, 6))  # blue

    # with open('C:\\Users\\n.kruchkov\\Desktop\\test_2.sxr', 'rb') as file:
    #     buff = struct.unpack('i', file.read(4))
    #     print(hex(buff[0]))
    file = open('C:\\Users\\n.kruchkov\\Desktop\\test_2.sxr', 'rb')

    # for MPI Data
    flag = True
    # while flag:
    for i in range(0, 641):
        for i in range(0, 1040):
            buff = file.read(4)
            if buff == '':
                flag = False
                break
            try:
                buff2 = struct.unpack('i', buff)[0]
            except struct.error:
                break
        # for byte_pos in range(0, len(file), 4):
        #     buff = struct.unpack('i', file.read(4))
        #     print(hex(buff))
            if buff2 >= 0xd387e640:
                f.write(convert(0xffffffffffff, 6)) #
            elif buff2 >= 0x77770000:
                f.write(convert(0xbbffbbbbffff, 6))  #
            # elif buff2 >= 0x33330000:
            #     f.write(convert(0x55ff5555ffff, 6))  #
            else:
                f.write(convert(0x00000000ffff, 6))  #
        f.write(convert(0x0000, 2))


    file.close()
    f.close()


            # 140 bytes
    # f.write(convert(0x0000ff0000ff, 6)) # red
    # f.write(convert(0xffffffffffff, 6)) # white
    # f.write(convert(0xe7bfc8e7bfc8, 6)) # grey
    # f.write(convert(0x0000, 2))
    #
    # f.write(convert(0x0000ff0000ff, 6)) # red
    # f.write(convert(0xffffffffffff, 6)) # white
    # f.write(convert(0xe7bfc8e7bfc8, 6)) # grey
    # f.write(convert(0x0000, 2))
    #
    # f.write(convert(0x00ff0000ff00, 6)) # green
    # f.write(convert(0x000000000000, 6)) # black
    # f.write(convert(0xff0080ff0080, 6)) # purple
    # f.write(convert(0x0000, 2))
    #
    # f.write(convert(0x00ff0000ff00, 6)) # green
    # f.write(convert(0x000000000000, 6)) # black
    # f.write(convert(0xff0080ff0080, 6)) # purple
    # f.write(convert(0x0000, 2))
    #
    # f.write(convert(0x0000ff0000ff, 6)) # red
    # f.write(convert(0xff8000ff8000, 6)) # blue
    # f.write(convert(0x0080ff0080ff, 6)) # orange
    # f.write(convert(0x0000, 2))
    #
    # f.write(convert(0x0000ff0000ff, 6)) # red
    # f.write(convert(0xff8000ff8000, 6)) # blue
    # f.write(convert(0x0080ff0080ff, 6)) # orange
    # f.write(convert(0x0000, 2))
    #
    # f.write(convert(0x0000ff0000ff, 6)) # red
    # f.write(convert(0xff8000ff8000, 6)) # blue
    # f.write(convert(0x0080ff0080ff, 6)) # orange
    # f.write(convert(0x0000, 2))
