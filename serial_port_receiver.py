import serial
import time
import datetime

DEBUG = 1


def serve_serial_port(com_port):
    ser = serial.Serial()
    
    '''Открываем порт с параметрами 57600-8-N-1'''
    ser.port = com_port
    ser.timeout = 10
    ser.baudrate = 57600
    ser.bytesize = 8
    ser.stopbits = 1
    ser.parity = 'N'
    ser.open()

    while 1:
        # ser = serial.Serial()
        # ser.baudrate = 57600
        # ser.port = 'COM2'
        # ser.timeout = 5
        # ser.parity = 'N'
        # ser.stopbits = 1
        # ser.bytesize = 8
        # ser.open()
        # ser.write(b'Hello!\n')
        # ser.write(str(datetime.datetime.utcnow())[:-7].encode())
        # ser.write(b'\n\n')
        # bel = time.localtime()
        # hour, mins, sec = bel.tm_hour, bel.tm_min, bel.tm_sec
        # mday, mon, year = bel.tm_mday, bel.tm_mon, bel.tm_year
        # # print(f'{bel.tm_hour}:{bel.tm_min}:{bel.tm_sec} - {bel.tm_mday}.{bel.tm_mon}.{bel.tm_year}')
        # print(f'{hour:02d}:{mins:02d}:{sec:02d} - {mday:02d}.{mon:02d}.{year:04d}')
        # print(ser.is_open)
        # time.sleep(1.0)

        reply = b''
        text = ser.read(40)
        # print('Message:', text)
        if len(text) == 40:
            reply = b'\x55\xAA'
            ser.write(reply)
            return text
        elif len(text) > 0:
            reply = b'\x99\x99'
            ser.write(reply)
            return 'Wrong message size'
        if reply != b'':
            ser.write(reply)
            print('Reply:', reply)
            print('\n')


if __name__ == '__main__':
    while 1:
        print(serve_serial_port())
