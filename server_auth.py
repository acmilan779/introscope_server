'''Это основной .py-модуль ПО для интроскопов.
Код, содержащийся в этом файле позволяет успешно проходить тест "Имитатор" СС ТМК.
Данный код конвертируется в .exe через модуль PyInstaller (если он скачан) путем запуска build.bat
'''

import socket
from functools import lru_cache
from urllib.parse import parse_qs, urlparse
from requests import Session

import threading
from datetime import datetime
import time
import sys
import os
import json

import body_XMLs
import tcp_logger
import http_uptime


class Req:

    def __init__(self,
                 method=None, url=None, headers=None, files=None, data=None,
                 params=None, auth=None, cookies=None, hooks=None, json=None,
                 path_url=None, body=None):
        # Default empty dicts for dict params.
        data = [] if data is None else data
        files = [] if files is None else files
        headers = {} if headers is None else headers
        params = {} if params is None else params
        hooks = {} if hooks is None else hooks

        self.hooks = hooks
        self.method = method
        self.url = url
        self.path_url = path_url
        self.body = body
        self.headers = headers
        self.files = files
        self.data = data
        self.json = json
        self.params = params
        self.auth = auth
        self.cookies = cookies

    def __repr__(self):
        return '<PreparedRequest [%s]>' % self.method


def data_split(data_raw):

    def key_list(k_dict, pos):
        k_list = list(k_dict.keys())
        element = [k_list[pos], k_dict[k_list[pos]]]
        return element

    data = ''
    data_structure = ['name', 'age']
    data += '='.join([data_structure[0], key_list(data_raw, 0)[0]])
    data += '&'
    data += '='.join([data_structure[1], key_list(data_raw, 0)[1]])
    return data


def do_request(URL, method, hooks, body, headers, path, data):
    request = Req(headers=headers, method=method,
                  path_url=path,
                  url=URL + path + '?' + data,
                  body=body, hooks=hooks)
    response = Session().send(request)
    return response


def request_post(URL, headers, path, data, hooks=None, body=None):
    response = do_request(URL, method='POST', headers=headers, path=path, data=data,
                          hooks=hooks, body=body)
    print('Response code:', response.status_code, response.reason)
    # print(response.elapsed)
    return response


'''Создаем класс, который будет запускать BagScan при запуске сервера'''
class ThreadBagScan(threading.Thread):
    def run(self):
        os.system('D:/BagVision/Client/BagScan/BagScan.exe')
        pass


'''Класс, который запускает TCP Logger на Интроскопе/Трио'''
class ThreadTCP(threading.Thread):
    def __init__(self, device):
        super(ThreadTCP, self).__init__()
        self.device = device
        self.alarm_message_dict = {}

    def run(self):
        '''TCP Logger Init'''
        serv_sock_tcp = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM,
            proto=0)
        init_key = False
        # print(self.dtime())
        while init_key is not True:
            error_key = False
            try:
                serv_sock_tcp.bind((host_ip, 23))
            except OSError as e:
                error_key = True
                print('TCP Logger: ', e.strerror)
                print('TCP Logger rebooting...')
                time.sleep(1.0)
            finally:
                if error_key is False:
                    init_key = True
        print('TCP Logger starting...')
        # pass

        try:
            serv_sock_tcp.listen()
            while True:
                # serv_sock_tcp.settimeout(2)

                try:
                    tcp_conn = serv_sock_tcp.accept()[0]
                    tcp_logger.serve_tcp_logger(tcp_conn, self.device)
                except socket.error as e:
                    print('TCP Logger: ', e)
                    pass

        finally:
            print('Serving TCP Logger ended')
            serv_sock_tcp.close()
            # self.run()


'''Класс, который запускает HTTP-сервер с отображением
времени работы интроскопа после включения'''
class ThreadHTTP(threading.Thread):
    def __init__(self, _host, _port, _model, _serial, _firmware):
        super(ThreadHTTP, self).__init__()
        self.host = _host
        self.port = _port
        self.model = _model
        self.serial = _serial
        self.firmware = _firmware

    def run(self):
        print('HTTP Run Here')
        while True:
            try:
                print('Starting HTTP Server...')
                http_uptime.start_http()

            except OSError as e:
                print('===\n\n')
                print(e)
                print('\n\n\n')
                pass
            finally:
                print('HTTP Uptime Server closed')


'''Класс, который запускает COM Logger на Trio'''
class ThreadCOM(threading.Thread):
    def __init__(self, device):
        super(ThreadCOM, self).__init__()
        self.device = device
        self.alarm_message_dict = {}

    def run(self):
        while True:
            try:
                print('Starting COM Logger...')
                tcp_logger.tcp_logger_trio_thread()
            finally:
                print('Serving COM Tread ended')


'''При запуске сервера считываем данные из конфига (IP, порт и характеристики интроскопа)'''
with open('C:/Introscopy_server/dist/config.json', 'r') as file:
    file_d = file.read()
data = json.loads(file_d)
host_ip = data['ip_adress']
host_port = int(data['port'])
host_http_port = int(data['http_port'])
model = data['model']
serial_number = data['serial_number']
firmware_ver = data['firmware_version']
device_type = data['device']

warn_path = r'C:/Introscopy_server/dist/resources/warn.bmp'

MAX_LINE = 64 * 1024
MAX_HEADERS = 100


class MyHTTPServer:
    def __init__(self, _host, _port, server_name):
        self._host = host
        self._port = port
        self._server_name = server_name
        self.remote_host = '0.0.0.0'
        self.remote_port = '80'
        self.subs_address = '/'
        self.send_event_key = False
        self.subs_key = False
        self.device_key = ''
        self.body_topic = ''

    def dtime(self):
        dtime = str(datetime.now())[:-7]
        dtime = dtime.replace(' ', 'T') + 'Z'
        return dtime

    def serve_forever(self):
        ## '''CC TMK Init'''
        serv_sock = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM,
            proto=0)
        init_key = False
        print(self.dtime())
        print('CC TMK:\n   Waiting for client to connect...')

        '''init_key запускает цикл, который ждет подлючения клиента'''
        while init_key is not True:
            error_key = False
            try:
                error_key = False
                serv_sock.bind((self._host, self._port))
            except OSError as e:
                error_key = True
                print('CC TMK: ', e.strerror)
                # try:
                #     error_key = False
                #     serv_sock.bind((self._host, 23))
                # except OSError as e:
                #     print('TCP Logger: ', e.strerror)
                time.sleep(1.0)
            finally:
                if error_key is False:
                    init_key = True
        print(f'Server starting on IP {self._host}\n')

        try:
            serv_sock.listen()
            # serv_sock_tcp.listen()
            while True:
                '''При запросе на подписку и генерацию события, приложение должно само сделать POST-запрос.
                Для обозначения этого момента используется ключ send_event_key
                '''
                try:
                    if self.send_event_key is False:
                        conn, _ = serv_sock.accept()
                        serv_sock.settimeout(None)
                        try:
                            self.serve_client(conn)
                        except Exception as e:
                            print('Client serving failed', e)
                    else:
                        self.handle_event_send_post()
                except socket.error as e:
                    serv_sock.settimeout(5)
                    print('CC TMK: ', e)
                    pass

        finally:
            print('Serving ended')
            serv_sock.close()

    '''Обрабатываем данные, полученные по TCP в serv_sock.accept'''
    def serve_client(self, conn):
        request = None
        try:
            request = self.parse_request(conn)
            response = self.handle_request(request)
            self.send_response(conn, response)
        except ConnectionResetError:
            conn = None
        except StopIteration:
            pass
        except Exception as e:
            self.send_error(conn, e)
            print('<Exception from serve_client>', e)

        if conn:
            if request:
                request.rfile.close()
            conn.close()

    '''Проходимся по всему полученному пакету данных и разбиваем на фрагменты (запрос, заголовки, тело)'''
    def parse_request(self, conn):
        rfile = conn.makefile('rb')
        method, target, ver = self.parse_request_line(rfile)
        headers = self.parse_headers(rfile)
        if '/event_service' in target or '/device_service' in target:
            buff = int(headers['Content-Length'])
            body = self.parse_body(rfile, buff)
        else:
            body_len, body = None, 'None'
        host = headers.get('Host')
        if not host:
            raise HTTPError(400, 'Bad request',
                            'Host header is missing')
        # print('Host name is', self._server_name, 'on port', self._port)
        if self._server_name not in headers.values() and \
                f'{self._server_name}:{self._port}' not in headers.values() and \
                f'{self._host}:{self._port}' not in headers.values():
            raise HTTPError(404, 'Not found')
        return Request(method, target, ver, headers, body, rfile)

    '''Анализируем первую строчку данных, определяем какой в ней HTTP-запрос (если он есть)'''
    def parse_request_line(self, rfile):
        raw = rfile.readline(MAX_LINE + 1)
        if len(raw) > MAX_LINE:
            raise HTTPError(400, 'Bad request',
                            'Request line is too long')
        req_line = str(raw, 'iso-8859-1')
        print('Request:', req_line.replace('\n', ''))
        words = req_line.split()
        if not words:
            raise StopIteration
        if len(words) != 3:
            raise HTTPError(400, 'Bad request',
                            'Malformed request line')
        method, target, ver = words
        if ver != 'HTTP/1.1':
            raise HTTPError(505, 'HTTP Version Not Supported')
        return method, target, ver

    '''Анализируем заголовки к запросу'''
    def parse_headers(self, rfile):
        headers_raw = []
        headers = {}
        while True:
            line = rfile.readline(MAX_LINE + 1)
            if len(line) > MAX_LINE:
                raise HTTPError(494, 'Request header too large')
            if line in (b'\r\n', b'\n', b''):
                break
            line = line.decode('iso-8859-1').replace('\n', '').replace('\r', '')
            headers_raw.append(line.split(': ', 1))
            if len(headers) > MAX_HEADERS:
                raise HTTPError(494, 'Too many host')

        headers_raw = [item for sublist in headers_raw for item in sublist]
        if len(headers_raw) % 2 == 0:
            for i in range(0, len(headers_raw), 2):
                headers[headers_raw[i]] = headers_raw[i + 1]
        # print('Headers:', headers)
        return headers

    '''Выделяем тело в запросе. По address_search определяем, является ли запрос подпиской на событие'''
    def parse_body(self, rfile, buff):
        body = rfile.read(buff)
        body_str = body.decode('iso-8859-1').replace('\n', '').replace('\r', '')
        address_search = '<ns2:ConsumerReference><Address>http://'
        if address_search in body_str:
            address_pos_start = body_str.find(address_search) + len(address_search)
            address_pos_end = body_str.find(f'</Address')
            address_pos = body_str.find(f'/', address_pos_start, address_pos_end)
            self.remote_host, self.remote_port = body_str[address_pos_start:address_pos].split(':')
            self.subs_address = body_str[address_pos:address_pos_end]
        return body

    '''После разбора пакета, определяем на что пришел запрос'''
    def handle_request(self, req):
        if req.path == '/onvif/device_service':
            return self.handle_get_device(req)

        if req.path == '/onvif/event_service':
            # print(type(req.body))
            if req.headers.get('SOAPAction') is not None:
                if 'GetServiceCapabilities' in req.headers.get('SOAPAction'):
                    return self.handle_get_serv_cap(req)
            elif 'SubscribeRequest' in req.body.decode('iso-8859-1'):
                return self.handle_event_subs(req)

        if req.path == '/event_service/0':
            if 'RenewRequest' in req.body.decode('iso-8859-1'):
                return self.handle_event_renew(req)
            elif 'UnsubscribeRequest' in req.body.decode('iso-8859-1'):
                return self.handle_event_unsubs(req)

        raise HTTPError(404, 'Not found')

    '''Отправляем ответное сообщение на запрос'''
    def send_response(self, conn, resp):
        wfile = conn.makefile('wb')
        status_line = f'HTTP/1.1 {resp.status} {resp.reason}\r\n'
        wfile.write(status_line.encode('iso-8859-1'))

        if resp.headers:
            for (key, value) in resp.headers:
                header_line = f'{key}: {value}\r\n'
                wfile.write(header_line.encode('iso-8859-1'))

        wfile.write(b'\r\n')
        if resp.body:
            wfile.write(resp.body)
        wfile.flush()
        wfile.close()

    '''Обрабатывает запросы по GetCapabilities, GetScopes и GetDeviceInformation'''
    def handle_get_device(self, req):
        contentType = req.headers.get('Content-Type')
        if 'GetCapabilities' in req.body.decode('iso-8859-1'):
            body = body_XMLs.resp_get_cap()
        elif 'GetScopes' in req.body.decode('iso-8859-1'):
            body = body_XMLs.resp_get_scopes()
        elif 'GetDeviceInformation' in req.body.decode('iso-8859-1'):
            body = body_XMLs.resp_get_dev_info()
        else:
            body = 'None'.encode('utf-8')
        headers = [('Content-Type', contentType),
                   ('Content-Length', len(body)),
                   ('Server', 'hsoap/2.8'),
                   ('Connection', 'close')]
        return Response(200, 'OK', headers=headers, body=body)

    '''Генерирует ответ на GetServiceCapabilities'''
    def handle_get_serv_cap(self, req):
        contentType = req.headers.get('Content-Type')
        action = req.headers.get('SOAPAction')
        if 'GetServiceCapabilities' in action:
            body = body_XMLs.resp_get_serv_cap()
            # body = body.encode('utf-8')
        else:
            body = 'None'.encode('utf-8')
        headers = [('Content-Type', contentType),
                   ('Content-Length', len(body)),
                   ('Server', 'hsoap/2.8'),
                   ('Connection', 'close')]
        return Response(200, 'OK', headers=headers, body=body)

    '''Определяет для какого устройства и какое событие нужно сгенерировать'''
    def handle_event_send_post(self):
        self.send_event_key = False
        path_event = self.subs_address
        URL = f'http://{self.remote_host}:{self.remote_port}'
        data = ''
        dtime = self.dtime()
        device, state, source, body_data = body_XMLs.parse_body(self.body_topic, warn_path,
                                                                thread_tcp.alarm_message_dict)
        print(f'Event: {device}/{state}')
        body = f'''<?xml version="1.0" encoding="UTF-8"?>
                            <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
                            xmlns:e="http://www.w3.org/2003/05/soap-encoding"
                            xmlns:wsa="http://www.w3.org/2005/08/addressing"
                            xmlns:xs="http://www.w3.org/2001/XMLSchema"
                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                            xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
                            xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
                            xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
                            xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
                            xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
                            xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
                            xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
                            xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
                            xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
                            xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
                            xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
                            xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
                            xmlns:tt="http://www.onvif.org/ver10/schema"
                            xmlns:tns1="http://www.onvif.org/ver10/topics"
                            xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
                            xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
                            xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
                            xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
                            xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
                            xmlns:dn="http://www.onvif.org/ver10/network/wsdl"
                            xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
                            xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
                            xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
                            xmlns:axt="http://www.onvif.org/ver20/analytics"
                            xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
                            xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
                            xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
                            xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
                            xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
                            xmlns:pt="http://www.onvif.org/ver10/pacs"
                            xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
                            xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
                            xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
                            xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
                            xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
                            xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
                            xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
                            xmlns:ter="http://www.onvif.org/ver10/error">
                                <s:Header>
                                    <wsa:Action>http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify</wsa:Action>
                                </s:Header>
                                <s:Body>
                                    <wsnt:Notify>
                                        <wsnt:NotificationMessage>
                                        <wsnt:Topic Dialect="http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet">
                                            tns1:Device/{device}/{state}
                                        </wsnt:Topic>
                                        <wsnt:Message>
                                            <tt:Message UtcTime="{dtime}" PropertyOperation="Initialized">
                                                <tt:Source>
                                                    {source}
                                                </tt:Source>
                                                <tt:Data>
                                                    {body_data}
                                                </tt:Data>
                                            </tt:Message>
                                        </wsnt:Message>
                                        </wsnt:NotificationMessage>
                                    </wsnt:Notify>
                                </s:Body>
                            </s:Envelope>'''
        headers = {'Content-Type': 'application/soap+xml; charset=utf-8; '
                                   'action="http://docs.oasis-open.org/wsn/bw-2/NotificationConsumer/Notify"',
                   'Content-Length': len(body),
                   'Host': f'{host_ip}:{host_port}',
                   'User-Agent': 'pycharm/1.0',
                   'Connection': 'close'}
        request_post(URL, headers, path=path_event, data=data, body=body)

    '''Генерирует ответ на запрос о подписке на события (Subscribe)'''
    def handle_event_subs(self, req):
        self.subs_key = True
        self.send_event_key = True
        body_text = req.body.decode('iso-8859-1')
        if 'TopicExpression' in body_text:
            self.body_topic = body_text.split('TopicExpression')[1]
        contentType = req.headers.get('Content-Type')
        dtime = self.dtime()
        resp_event_subs = f'''<?xml version="1.0" encoding="UTF-8"?>
                    <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
                    xmlns:e="http://www.w3.org/2003/05/soap-encoding"
                    xmlns:wsa="http://www.w3.org/2005/08/addressing"
                    xmlns:xs="http://www.w3.org/2001/XMLSchema"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
                    xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
                    xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
                    xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
                    xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
                    xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
                    xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
                    xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
                    xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
                    xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
                    xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
                    xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
                    xmlns:tt="http://www.onvif.org/ver10/schema"
                    xmlns:tns1="http://www.onvif.org/ver10/topics"
                    xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
                    xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
                    xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
                    xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
                    xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
                    xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
                    xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
                    xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
                    xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
                    xmlns:axt="http://www.onvif.org/ver20/analytics"
                    xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
                    xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
                    xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
                    xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
                    xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
                    xmlns:pt="http://www.onvif.org/ver10/pacs"
                    xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
                    xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
                    xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
                    xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
                    xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
                    xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
                    xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
                    xmlns:ter="http://www.onvif.org/ver10/error">
                        <s:Header>
                            <wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
                            <wsa:Action>http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeResponse</wsa:Action>
                        </s:Header>
                        <s:Body>
                            <wsnt:SubscribeResponse>
                                <wsnt:SubscriptionReference>
                                    <wsa:Address>http://{host_ip}:{host_port}/event_service/0</wsa:Address>
                                </wsnt:SubscriptionReference>
                                <wsnt:CurrentTime>{dtime}</wsnt:CurrentTime>
                                <wsnt:TerminationTime>{dtime}</wsnt:TerminationTime>
                            </wsnt:SubscribeResponse>
                        </s:Body>
                    </s:Envelope>'''
        body = resp_event_subs.encode('utf-8')
        headers = [('Content-Type', contentType),
                   ('Content-Length', len(body)),
                   ('Server', 'hsoap/2.8'),
                   ('Connection', 'close')]
        return Response(200, 'OK', headers=headers, body=body)

    '''Генерирует ответ на запрос об обновлении подписки (Renew)'''
    def handle_event_renew(self, req):
        contentType = req.headers.get('Content-Type')
        dtime = self.dtime()
        msg = 'null'
        resp_event_renew = f'''<?xml version="1.0" encoding="UTF-8"?>
                                            <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
                                            xmlns:e="http://www.w3.org/2003/05/soap-encoding"
                                            xmlns:wsa="http://www.w3.org/2005/08/addressing"
                                            xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                            xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
                                            xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
                                            xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
                                            xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
                                            xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
                                            xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
                                            xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
                                            xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
                                            xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
                                            xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
                                            xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
                                            xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
                                            xmlns:tt="http://www.onvif.org/ver10/schema"
                                            xmlns:tns1="http://www.onvif.org/ver10/topics"
                                            xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
                                            xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
                                            xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
                                            xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
                                            xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
                                            xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
                                            xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
                                            xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
                                            xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
                                            xmlns:axt="http://www.onvif.org/ver20/analytics"
                                            xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
                                            xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
                                            xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
                                            xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
                                            xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
                                            xmlns:pt="http://www.onvif.org/ver10/pacs"
                                            xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
                                            xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
                                            xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
                                            xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
                                            xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
                                            xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
                                            xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
                                            xmlns:ter="http://www.onvif.org/ver10/error">
                                                <s:Header>
                                                    <wsa:MessageID>uuid:{msg}</wsa:MessageID>
                                                    <wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
                                                    <wsa:Action>http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/RenewResponse</wsa:Action>
                                                </s:Header>
                                                    <s:Body>
                                                    <wsnt:RenewResponse>
                                                        <wsnt:TerminationTime>{dtime}</wsnt:TerminationTime>
                                                        <wsnt:CurrentTime>{dtime}</wsnt:CurrentTime>
                                                    </wsnt:RenewResponse>
                                                </s:Body>
                                            </s:Envelope>'''
        body = resp_event_renew.encode('utf-8')
        headers = [('Content-Type', contentType),
                   ('Content-Length', len(body)),
                   ('Server', 'hsoap/2.8'),
                   ('Connection', 'close')]
        return Response(200, 'OK', headers=headers, body=body)

    '''Генерирует ответ на запрос об отмене подписки (Unsubscribe)'''
    def handle_event_unsubs(self, req):
        self.subs_key = False
        contentType = req.headers.get('Content-Type')
        msg = 'null'  # Произвольный номер
        resp_event_unsubs = f'''<?xml version="1.0" encoding="UTF-8"?>
                                            <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
                                            xmlns:e="http://www.w3.org/2003/05/soap-encoding"
                                            xmlns:wsa="http://www.w3.org/2005/08/addressing"
                                            xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                            xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl"
                                            xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2"
                                            xmlns:wstop="http://docs.oasis-open.org/wsn/t-1"
                                            xmlns:wsntw="http://docs.oasis-open.org/wsn/bw-2"
                                            xmlns:wsrf-rw="http://docs.oasis-open.org/wsrf/rw-2"
                                            xmlns:wsrf-r="http://docs.oasis-open.org/wsrf/r-2"
                                            xmlns:wsrf-bf="http://docs.oasis-open.org/wsrf/bf-2"
                                            xmlns:wsdl="http://schemas.xmlsoap.org/wsdl"
                                            xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12"
                                            xmlns:http="http://schemas.xmlsoap.org/wsdl/http"
                                            xmlns:d="http://schemas.xmlsoap.org/ws/2005/04/discovery"
                                            xmlns:wsadis="http://schemas.xmlsoap.org/ws/2004/08/addressing"
                                            xmlns:tt="http://www.onvif.org/ver10/schema"
                                            xmlns:tns1="http://www.onvif.org/ver10/topics"
                                            xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
                                            xmlns:trt="http://www.onvif.org/ver10/media/wsdl"
                                            xmlns:tev="http://www.onvif.org/ver10/events/wsdl"
                                            xmlns:timg="http://www.onvif.org/ver20/imaging/wsdl"
                                            xmlns:tst="http://www.onvif.org/ver10/storage/wsdl"
                                            xmlns:dn="http://www.onvif.org/ver10/network/wsdl" 
                                            xmlns:tr2="http://www.onvif.org/ver20/media/wsdl"
                                            xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"
                                            xmlns:tan="http://www.onvif.org/ver20/analytics/wsdl"
                                            xmlns:axt="http://www.onvif.org/ver20/analytics"
                                            xmlns:trp="http://www.onvif.org/ver10/replay/wsdl"
                                            xmlns:tse="http://www.onvif.org/ver10/search/wsdl"
                                            xmlns:trc="http://www.onvif.org/ver10/recording/wsdl"
                                            xmlns:tac="http://www.onvif.org/ver10/accesscontrol/wsdl"
                                            xmlns:tdc="http://www.onvif.org/ver10/doorcontrol/wsdl"
                                            xmlns:pt="http://www.onvif.org/ver10/pacs"
                                            xmlns:tmd="http://www.onvif.org/ver10/deviceIO/wsdl"
                                            xmlns:tth="http://www.onvif.org/ver10/thermal/wsdl"
                                            xmlns:tcr="http://www.onvif.org/ver10/credential/wsdl"
                                            xmlns:tar="http://www.onvif.org/ver10/accessrules/wsdl"
                                            xmlns:tsc="http://www.onvif.org/ver10/schedule/wsdl"
                                            xmlns:trv="http://www.onvif.org/ver10/receiver/wsdl"
                                            xmlns:tpv="http://www.onvif.org/ver10/provisioning/wsdl"
                                            xmlns:ter="http://www.onvif.org/ver10/error">
                                                <s:Header>
                                                    <wsa:MessageID>uuid:{msg}</wsa:MessageID>
                                                    <wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
                                                    <wsa:Action>http://docs.oasis-open.org/wsn/bw-2/SubscriptionManager/UnsubscribeResponse</wsa:Action>
                                                </s:Header>
                                                <s:Body>
                                                    <wsnt:UnsubscribeResponse>
                                                    </wsnt:UnsubscribeResponse>
                                                </s:Body>
                                            </s:Envelope>'''
        body = resp_event_unsubs.encode('utf-8')
        headers = [('Content-Type', contentType),
                   ('Content-Length', len(body)),
                   ('Server', 'hsoap/2.8'),
                   ('Connection', 'close')]
        return Response(200, 'OK', headers=headers, body=body)

    def send_error(self, conn, err):
        try:
            status = err.status
            reason = err.reason
            body = (err.body or err.reason).encode('utf-8')
        except:
            status = 500
            reason = b'Internal Server Error'
            body = b'Internal Server Error'
        resp = Response(status, reason,
                        [('Content-Length', len(body))],
                        body)
        self.send_response(conn, resp)


class Request:
    def __init__(self, method, target, version, headers, body, rfile):
        self.method = method
        self.target = target
        self.version = version
        self.headers = headers
        self.body = body
        self.rfile = rfile

    @property
    def path(self):
        return self.url.path

    @property
    @lru_cache(maxsize=None)
    def query(self):
        return parse_qs(self.url.query)

    @property
    @lru_cache(maxsize=None)
    def url(self):
        return urlparse(self.target)

    def body(self):
        size = self.headers.get('Content-Length')
        if not size:
            return None
        return self.rfile.read(size)


class Response:
    def __init__(self, status, reason, headers=None, body=None):
        self.status = status
        self.reason = reason
        self.headers = headers
        self.body = body


class HTTPError(Exception):
    def __init__(self, status, reason, body=None):
        super()
        self.status = status
        self.reason = reason
        self.body = body


# '''При запуске сервера считываем данные из конфига (IP, порт и характеристики интроскопа)'''
# with open('C:/Introscopy_server/dist/config.json', 'r') as file:
#     file_d = file.read()
# data = json.loads(file_d)
# host_ip = data['ip_adress']
# host_port = int(data['port'])
# host_http_port = int(data['http_port'])
# model = data['model']
# serial_number = data['serial_number']
# firmware_ver = data['firmware_version']
# device_type = data['device']

# warn_path = r'C:/Introscopy_server/dist/resources/warn.bmp'

# MAX_LINE = 64 * 1024
# MAX_HEADERS = 100

'''main()
При запуске считывает параметры запуска из командной строки,
если их нет - берет значения IP и порта из конфига
'''
if __name__ == '__main__':

    '''Запускаем BagScan вместе с нашим серверным приложением'''
    thread = ThreadBagScan()
    thread.daemon = True
    if data['bagscan'] == '1':
        thread.start()
        print('Starting BagScan...')

    '''Запускаем TCP-логгер'''
    thread_tcp = ThreadTCP(device_type)
    thread_tcp.daemon = True
    thread_tcp.start()

    '''Запускаем HTTP-сервер'''
    if host_http_port != 0:
        thread_http_server = ThreadHTTP(host_ip, host_http_port, model, serial_number, firmware_ver)
        thread_http_server.daemon = True
        # print('HTTP Here')
        thread_http_server.start()

    '''Запускаем прослушивание COM-порта для рамки Trio'''
    thread_tcp_com = ThreadCOM(device_type)
    thread_tcp_com.daemon = True
    if data['device'] == 'Trio':
        thread_tcp_com.start()
        print('Starting COM-port...')

    host = None
    port = None

    try:
        host = sys.argv[1]
        port = int(sys.argv[2])
    except IndexError:
        if not host:
            host = host_ip
        if not port:
            port = host_port
    name = device_type

    '''Основной цикл сервера'''
    serv = MyHTTPServer(host, port, name)
    try:
        serv.serve_forever()
    except (KeyboardInterrupt, SystemExit):
        pass
    finally:
        print('Program was closed')
