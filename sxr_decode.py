import struct
import json

PATH_TO_BMP = "C:/Introscopy_server/dist/bmp_big.bmp"
PALETTE_3 = [  # // Orange
            [0, 0, 200],
            [250, 180, 0],
            [250, 180, 0],
            [250, 180, 0],
            [250, 180, 0],
            [250, 180, 0],
            [250, 180, 0],
            [250, 180, 0],
            [250, 180, 0],
            [250, 180, 0],
            [5, 250, 125],
            # // Green
            [5, 250, 125],
            [5, 250, 125],
            [5, 250, 125],
            [5, 250, 125],
            [5, 250, 125],
            [0, 0, 200],
            [0, 0, 200],
            # // Blue
            [0, 0, 200]
            ]

SXR0 = 0x30525853
RATIO_SIZE = 0x08
IMAGE_THRESHOLD = 10000

SEQ_CODE = 0x43
ELEM_CODE = 0x45
FIELD_CODE = 0x46
FIELD_CODE_2 = 0x5A
CODES_LIST = [SEQ_CODE, ELEM_CODE, FIELD_CODE, FIELD_CODE_2]

'''Convert sting to Big Endian'''
def convert(line, size):
    return (int(line)).to_bytes(size, byteorder="big")


'''Convert sting to Little Endian'''
def convert_little(line, size):
    return (int(line)).to_bytes(size, byteorder="little")


class SXR:

    def __init__(self, path):
        self.path_to_sxr = path
        '''Global variables'''
        self.file_buff = 0
        self.leaves_list = []
        self.width, self.height = 0, 0

    '''Save array as BMP file'''
    def create_bmp(self, to_write_array):

        with open(PATH_TO_BMP, "w+b") as f:

            '''Header for BMP-file'''
            f_size = self.width * self.height * 6 + 54
            padding_size = self.width % 4
            f.write(convert(0x424D, 2))  # BM
            f.write(convert_little(f_size, 4))  # file size (bytes)
            f.write(convert(0, 4))  # reserved
            f.write(convert(0x36000000, 4))  # offset of img data
            f.write(convert(0x28000000, 4))  # DIB hdr size
            f.write(convert_little(self.width, 4))  # width
            f.write(convert_little(self.height, 4))  # height
            f.write(convert(0x0100, 2))  # planes
            f.write(convert(0x1800, 2))  # bits/px
            f.write(convert(0, 4))  # compress
            f.write(convert(0x78000000, 4))  # img size (bytes)
            f.write(convert(0, 4))  # x resolution
            f.write(convert(0, 4))  # y resolution
            f.write(convert(0, 4))  # #/colors
            f.write(convert(0, 4))  # #/important colors

            '''Write to_image_array data to file after BMP-header'''
            for y in range(self.height - 1, -1, -1):
                for x in range(0, self.width):
                    dot = to_write_array[x][y]
                    f.write(convert_little(dot, 3))

                if self.width % 4 != 0:
                    for i in range(0, self.width % 4):
                        f.write(convert_little(padding_size, 1))

            f.close()

    '''Calculate to_write_array for 1 View device'''
    def create_bmp_1_view(self, intensity_array, atom_mass_array):

        with open('C:/Introscopy_server/dist/config.json', 'r') as file:
            file_d = file.read()

        '''Read a contrast factor from the config'''
        data = json.loads(file_d)
        depth_factor = float(data['contrast'])
        intens_avr = sum(sum(line) for line in intensity_array)
        intens_avr /= len(intensity_array) * len(intensity_array[0])

        '''Filling to_write_array with data for each pixels'''
        to_write_array = [[0 for x in range(0, self.height)] for y in range(0, self.width)]
        red, green, blue = 0, 0, 0
        for y in range(self.height-1, -1, -1):
            for x in range(0, self.width):
                atom = atom_mass_array[x][y]

                if atom < 0.0:
                    atom = 0.0

                elif atom < 18.0:
                    red = PALETTE_3[int(atom)][0]
                    green = PALETTE_3[int(atom)][1]
                    blue = PALETTE_3[int(atom)][2]

                else:
                    red = PALETTE_3[18][0]
                    green = PALETTE_3[18][1]
                    blue = PALETTE_3[18][2]

                depth = intensity_array[x][y]
                depth = (depth - intens_avr) * depth_factor + intens_avr

                '''Overflow check'''
                if depth > 65535:
                    depth = 65535
                if depth < 0:
                    depth = 0

                if depth < 4080:
                    depth /= 16
                elif depth < 32640:
                    depth /= 128
                else:
                    depth /= 255
                depth *= 255

                if depth > 65535:
                    depth = 65535
                if depth < 0:
                    depth = 0

                if depth < 32768:
                    coef = depth / 32768
                    blue = float(255 - (255 - blue) * coef)
                    green = float(255 - (255 - green) * coef)
                    red = float(255 - (255 - red) * coef)
                else:
                    coef = (2 - depth / 32768)
                    blue = float(blue * coef)
                    green = float(green * coef)
                    red = float(red * coef)

                if red > 255:
                    red = 255.0
                if green > 255:
                    green = 255.0
                if blue > 255:
                    blue = 255.0
                if red < 0:
                    red = 0.0
                if green < 0:
                    green = 0.0
                if blue < 0:
                    blue = 0.0

                to_write_array[x][y] = (int(red) << 16) + (int(green) << 8) + int(blue)

        self.create_bmp(to_write_array)

    '''Calculate to_write_array for 2 View device'''
    def create_bmp_2_view(self, intensity_array, atom_mass_array):

        with open('C:/Introscopy_server/dist/config.json', 'r') as file:
            file_d = file.read()

        '''Read a contrast factor from the config'''
        data = json.loads(file_d)
        depth_factor = float(data['contrast'])
        intens_avr = sum(sum(line) for line in intensity_array)
        intens_avr /= len(intensity_array) * len(intensity_array[0])

        '''Double the image width as it will be 2 View image'''
        self.width *= 2

        '''Filling to_write_array with data for each pixels'''
        to_write_array = [[0 for x in range(0, self.height)] for y in range(0, self.width)]
        red, green, blue = 0, 0, 0
        for y in range(self.height - 1, -1, -1):
            for x in range(0, self.width):
                atom = atom_mass_array[x][y]
                if atom < 0.0:
                    atom = 0.0

                elif atom < 18.0:
                    red = PALETTE_3[int(atom)][0]
                    green = PALETTE_3[int(atom)][1]
                    blue = PALETTE_3[int(atom)][2]

                else:
                    red = PALETTE_3[18][0]
                    green = PALETTE_3[18][1]
                    blue = PALETTE_3[18][2]

                depth = intensity_array[x][y]
                depth = (depth - intens_avr) * depth_factor + intens_avr

                '''Overflow check'''
                if depth > 65535:
                    depth = 65535
                if depth < 0:
                    depth = 0

                if depth < 4080:
                    depth /= 16
                elif depth < 32640:
                    depth /= 128
                else:
                    depth /= 255
                depth *= 255

                if depth > 65535:
                    depth = 65535
                if depth < 0:
                    depth = 0

                if depth < 32768:
                    coef = depth / 32768
                    blue = float(255 - (255 - blue) * coef)
                    green = float(255 - (255 - green) * coef)
                    red = float(255 - (255 - red) * coef)
                else:
                    coef = (2 - depth / 32768)
                    blue = float(blue * coef)
                    green = float(green * coef)
                    red = float(red * coef)

                if red > 255:
                    red = 255.0
                if green > 255:
                    green = 255.0
                if blue > 255:
                    blue = 255.0
                if red < 0:
                    red = 0.0
                if green < 0:
                    green = 0.0
                if blue < 0:
                    blue = 0.0

                to_write_array[x][y] = (int(red) << 16) + (int(green) << 8) + int(blue)

        self.create_bmp(to_write_array)

    '''Work with width, height and data block
    to create intensity and atom_mass arrays'''
    def convert_to_bmp_1_view(self):
        '''Init of two zero-filled arrays'''
        intensity_array = [[0 for x in range(0, self.height)] for y in range(0, self.width)]
        atom_mass_array = [[0 for x in range(0, self.height)] for y in range(0, self.width)]

        block_2_pos, block_2_size = self.leaves_list[1][0] + 12, self.leaves_list[1][1] - 4
        block_2 = self.file_buff[block_2_pos:(block_2_pos + block_2_size)]

        '''for Intensity and Atom Mass Data in Energy Intensity block'''
        dot_pos = 0
        for x in range(0, self.width * 2):
            for y in range(0, self.height):

                '''Check if it`s Intensity or Atom Mass line'''
                if x % 2 == 0:
                    try:
                        intensity_array[x // 2][y] = struct.unpack('f', block_2[dot_pos:dot_pos + 4])[0]
                    except IndexError:
                        print(f'IndexError in intensity\n[{x}][{y}]\n{dot_pos}')

                else:
                    try:
                        atom_mass_array[x // 2][y] = struct.unpack('f', block_2[dot_pos:dot_pos + 4])[0]
                    except IndexError:
                        print(f'IndexError in atom mass\n[{x}][{y}]\n{dot_pos}')
                    except struct:
                        print(f'StructError in atom mass\n[{x}][{y}]\n{dot_pos}')
                dot_pos += 4

        self.create_bmp_1_view(intensity_array, atom_mass_array)

    '''Work with width, height and data block
        to create intensity and atom_mass arrays'''
    def convert_to_bmp_2_view(self):
        '''Init of two zero-filled arrays'''
        intensity_array = [[0 for x in range(0, self.height)] for y in range(0, 2 * self.width)]
        atom_mass_array = [[0 for x in range(0, self.height)] for y in range(0, 2 * self.width)]
        intensity_array_1 = [[0 for x in range(0, self.height)] for y in range(0, self.width)]
        atom_mass_array_1 = [[0 for x in range(0, self.height)] for y in range(0, self.width)]
        intensity_array_2 = [[0 for x in range(0, self.height)] for y in range(0, self.width)]
        atom_mass_array_2 = [[0 for x in range(0, self.height)] for y in range(0, self.width)]

        '''Blocks 2 and 5 contains raw unfiltered images'''
        # block_2_pos, block_2_size = self.leaves_list[1][0] + 12, self.leaves_list[1][1] - 4
        # block_5_pos, block_5_size = self.leaves_list[4][0] + 12, self.leaves_list[4][1] - 4

        '''Blocks 3 and 6 contains images that were sharpened by BagScan'''
        block_3_pos, block_3_size = self.leaves_list[2][0] + 12, self.leaves_list[2][1] - 4
        block_6_pos, block_6_size = self.leaves_list[5][0] + 12, self.leaves_list[5][1] - 4
        block_1 = self.file_buff[block_3_pos:(block_3_pos + block_3_size)]
        block_2 = self.file_buff[block_6_pos:(block_6_pos + block_6_size)]

        '''For Intensity and Atom Mass Data in Energy Intensity block'''
        '''For block_1 (3-rd of 6)'''
        dot_pos = 0
        for x in range(0, self.width * 2):
            for y in range(0, self.height):

                '''Check if it`s Intensity or Atom Mass line'''
                if x % 2 == 0:
                    try:
                        intensity_array_1[x // 2][y] = struct.unpack('f', block_1[dot_pos:dot_pos + 4])[0]
                    except IndexError:
                        print(f'IndexError in intensity\n[{x}][{y}]\n{dot_pos}')

                else:
                    try:
                        atom_mass_array_1[x // 2][y] = struct.unpack('f', block_1[dot_pos:dot_pos + 4])[0]
                    except IndexError:
                        print(f'IndexError in atom mass\n[{x}][{y}]\n{dot_pos}')
                    except struct:
                        print(f'StructError in atom mass\n[{x}][{y}]\n{dot_pos}')

                dot_pos += 4

        '''For block_2 (6-th of 6)'''
        dot_pos = 0
        for x in range(0, self.width * 2):
            for y in range(0, self.height):

                '''Check if it`s Intensity or Atom Mass line'''
                if x % 2 == 0:
                    try:
                        intensity_array_2[x // 2][y] = struct.unpack('f', block_2[dot_pos:dot_pos + 4])[0]
                    except IndexError:
                        print(f'IndexError in intensity\n[{x}][{y}]\n{dot_pos}')

                else:
                    try:
                        atom_mass_array_2[x // 2][y] = struct.unpack('f', block_2[dot_pos:dot_pos + 4])[0]
                    except IndexError:
                        print(f'IndexError in atom mass\n[{x}][{y}]\n{dot_pos}')
                    except struct:
                        print(f'StructError in atom mass\n[{x}][{y}]\n{dot_pos}')

                dot_pos += 4

        '''Merge two blocks in one twice-wide block'''
        for x in range(0, self.width * 2):
            for y in range(0, self.height):

                if x < self.width:
                    intensity_array[x][y] = intensity_array_1[x][y]
                    atom_mass_array[x][y] = atom_mass_array_1[x][y]

                else:
                    intensity_array[x][y] = intensity_array_2[x-self.width][y]
                    atom_mass_array[x][y] = atom_mass_array_2[x-self.width][y]

        self.create_bmp_2_view(intensity_array, atom_mass_array)

    '''Main method that should be called by extern programs'''
    def convert_to_bmp(self):

        '''Read all file'''
        with open(self.path_to_sxr, 'rb') as file:
            self.file_buff = file.read()

        '''Verify file format'''
        if struct.unpack('i', self.file_buff[0:4])[0] != SXR0:
            print('Not a SXR file')
            exit(-3)

        leaves_list_buff = []
        pos_current = 0x08
        pos_next = 0x10
        item_next = \
            item_current = self.file_buff[pos_current]
        '''Find all elements that not in 8 Bytes chain and 
        save their addresses and sizes as tuple in leaves_list'''
        try:
            while pos_current <= len(self.file_buff):
                item_current = item_next
                item_next = self.file_buff[pos_next]

                if item_next not in CODES_LIST:
                    block_size = struct.unpack('i', self.file_buff[pos_current + 4:pos_current + 8])[0]
                    leaves_list_buff.append([pos_current, block_size])
                    pos_next = pos_current + 8 + block_size
                    item_next = self.file_buff[pos_next]

                else:
                    pos_current = pos_next
                    item_next = self.file_buff[pos_next]
                    pos_next += 8

        except IndexError:
            '''Search for image data blocks, width and height '''
            for item in leaves_list_buff:
                if item[1] > 10000:
                    self.leaves_list.append(item)
                    print(item)

                elif item[1] == 8:
                    self.width = struct.unpack('i', self.file_buff[item[0] + 8:item[0] + 12])[0]
                    self.height = struct.unpack('i', self.file_buff[item[0] + 12:item[0] + 16])[0]

            meta_address = leaves_list_buff[-3]
            meta_data = self.file_buff[meta_address[0]+8:meta_address[0]+meta_address[1]+8]

            if b'markedAsDangerous' in meta_data:
                print('Dangerous!')
            else:
                print('Nothing found')


        '''Check if it`s 1 View or 2 View device'''
        if len(self.leaves_list) == 2 or len(self.leaves_list) == 3:
            self.convert_to_bmp_1_view()
        elif len(self.leaves_list) == 6:
            self.convert_to_bmp_2_view()
        else:
            print('Failed to obtain file structure')
            exit(-3)

        '''Clear the buffer where all SXR data was stored'''
        del self.file_buff

        return PATH_TO_BMP

    '''Check if there is 'markAsDangerous' in sxr-metadata'''
    def check_metadata(self):

        sxr_file = self.path_to_sxr.split('\\')[-1]

        '''Read all file'''
        with open(self.path_to_sxr, 'rb') as file:
            self.file_buff = file.read()

        '''Verify file format'''
        if struct.unpack('i', self.file_buff[0:4])[0] != SXR0:
            print('Not a SXR file')
            exit(-3)

        leaves_list_buff = []
        pos_current = 0x08
        pos_next = 0x10
        item_next = \
            item_current = self.file_buff[pos_current]
        '''Find all elements that not in 8 Bytes chain and 
        save their addresses and sizes as tuple in leaves_list'''
        try:
            while pos_current <= len(self.file_buff):
                item_current = item_next
                item_next = self.file_buff[pos_next]

                if item_next not in CODES_LIST:
                    block_size = struct.unpack('i', self.file_buff[pos_current + 4:pos_current + 8])[0]
                    leaves_list_buff.append([pos_current, block_size])
                    pos_next = pos_current + 8 + block_size
                    item_next = self.file_buff[pos_next]

                else:
                    pos_current = pos_next
                    item_next = self.file_buff[pos_next]
                    pos_next += 8
        except IndexError:
            '''Search for image data blocks, width and height '''
            meta_address = leaves_list_buff[-3]
            meta_data = self.file_buff[meta_address[0] + 8:meta_address[0] + meta_address[1] + 8]

            if b'markedAsDangerous' in meta_data:
                print('Dangerous!')
                sxr_file = '[Dangerous]' + sxr_file

        return sxr_file

    '''Check version of BagScan'''
    def check_bagscan_version(self):

        sxr_file = self.path_to_sxr.split('\\')[-1]
        '''Read all file'''
        with open(self.path_to_sxr, 'rb') as file:
            self.file_buff = file.read()

        '''Verify file format'''
        if struct.unpack('i', self.file_buff[0:4])[0] != SXR0:
            print('Not a SXR file')
            exit(-3)

        leaves_list_buff = []
        pos_current = 0x08
        pos_next = 0x10
        item_next = \
            item_current = self.file_buff[pos_current]
        '''Find all elements that not in 8 Bytes chain and 
        save their addresses and sizes as tuple in leaves_list'''
        try:
            while pos_current <= len(self.file_buff):
                item_current = item_next
                item_next = self.file_buff[pos_next]

                if item_next not in CODES_LIST:
                    block_size = struct.unpack('i', self.file_buff[pos_current + 4:pos_current + 8])[0]
                    leaves_list_buff.append([pos_current, block_size])
                    pos_next = pos_current + 8 + block_size
                    item_next = self.file_buff[pos_next]

                else:
                    pos_current = pos_next
                    item_next = self.file_buff[pos_next]
                    pos_next += 8
        except IndexError:
            '''Search for image data blocks, width and height '''
            meta_address = leaves_list_buff[-3]
            meta_data = self.file_buff[meta_address[0] + 8:meta_address[0] + meta_address[1] + 8]
            version = meta_data.split(b'softwareVersion')[1]
            print(version)
            return version
