import socket
import struct
import time
import datetime
import os
import win32event, win32file, win32con
import json

import serial_port_receiver
import sxr_decode

'''Считываем данные из конфига '''
with open(r'C:/Introscopy_server/dist/config.json', 'r') as file:
    file_d = file.read()
data = json.loads(file_d)
serial_number = data['serial_number']
model_number = data['model']
firmware_version = data['firmware_version']
wait_time = int(data['wait_time'])
device_model = data['model']
com_port = 'COM' + data['COM_port']

def get_int_time():
    return datetime.datetime.now().hour * 3600 + datetime.datetime.now().minute * 60 + datetime.datetime.now().second


global_alarm_dict = dict()
global_alarm_dict['level'] = 0
global_alarm_dict['zone'] = []
global_alarm_dict['rad_level'] = 0
global_alarm_dict['rad_zone'] = []
global_alarm_dict['explosives'] = 0
global_alarm_dict['time'] = get_int_time()

class ImageFile:

    def __init__(self, image_folder_path):
        self.image_folder_path = image_folder_path

    ##  добавляет к сообщению префикс с размером сообщения
    ##  пересылает сообщение по указанному сокету
    def send_data(self, conn, msg):
        # print('Msg size = ', len(msg))
        msg = struct.pack('>I', len(msg)) + msg
        conn.send(msg)

    ##  использует send_data, чтобы передать название файла с изображением
    def send_name(self, image_name, conn):
        self.send_data(conn, image_name.encode())
        # conn.send(image_name.encode('utf-8'))
        # print('file name len: ',len(image_name.encode('utf-8')))

    ##  кодирует файл изображения в base64
    ##  и передает его используя send_data
    # def send_image(self, image_name, conn):
    #     file = open(self.image_folder_path + '\\' + image_name, 'rb')
    #     sxr = base64.b64encode(file.read(70*1024*1024))   # 70MB
    #     print(len(sxr))
    #     self.send_data(conn, sxr)
    #     file.close()


'''Поиск файла с логами 'UserActionsLog_dd.mm.yyyy hh-mm-ss-uuuuuuu.txt'
возвращает полное имя этого файла'''
def subdirs(path):
    for entry in os.scandir(path):
        if entry.name.startswith('UserActionsLog') and entry.is_file():
            return entry.name


def dtime():
    _dtime = str(datetime.datetime.now())[:-7]
    return _dtime


'''Инициализация и запуск tcp-сервера на порту 9000
возвращает созданный сокет'''
def tcp_init(_host='169.254.199.129', _port=23):
    # port = 9000
    sock = socket.socket()
    sock.bind((_host, _port))
    sock.listen(1)
    # print('Wait for client to connect...')
    conn, addr = sock.accept()
    print('TCP connected:', addr)
    return conn


'''Отправка сообщения в XML-оболочке'''
def send_message(_conn, _alarm_state, device):
    dtime_date_buff = dtime().split()[0].split('-')
    dtime_date = []
    for i in range(3):
        dtime_date.append(dtime_date_buff[2 - i])
    dtime_date = '.'.join(item for item in dtime_date)

    hours_local = time.localtime()[3]
    dtime_time_buff = dtime().split()[1].split(':')
    dtime_time_buff[0] = ('0' + str(hours_local)) if hours_local < 10 else str(hours_local)
    dtime_time = ':'.join(item for item in dtime_time_buff)

    if device == 'Introscopy':
        body = \
    f'''<?xml version = "1.0" encoding = "utf-8"?>
<introscope id="{serial_number}">
    <model value="SmartScan {model_number}"/>
    <software_version value="{firmware_version}"/>
    <date value="{dtime_date}"/>
    <time value="{dtime_time}"/>
    <options value="0"/>
    <message value="New object was scanned"/>
    <alarm value="{_alarm_state}"/>
</introscope>\n\n'''

    elif device == 'Trio':
        alarm_level = _alarm_state['level']
        alarm_zone = ' '.join(str(item) for item in _alarm_state['zone'])
        alarm_rad_lvl = _alarm_state['rad_level']
        alarm_rad_zone = ' '.join(str(item) for item in _alarm_state['rad_zone'])
        alarm_explosives = _alarm_state['explosives']
        body = \
    f'''<?xml version = "1.0" encoding = "utf-8"?>
<detector id="{serial_number}">
    <model value="SmartScan IntelliMax {device}"/>
    <software_version value="{firmware_version}"/>
    <date value="{dtime_date}"/>
    <time value="{dtime_time}"/>
    <alarm>
        <level value="{alarm_level}"/>
        <zone value="{alarm_zone}"/>
        <rad_lvl value="{alarm_rad_lvl}"/>
        <rad_zone value="{alarm_rad_zone}"/>
        <explosives value="{alarm_explosives}"/>
    </alarm>
</detector> \n\n'''
    else:
        body = '<?xml version = "1.0" encoding = "utf-8"?>\n\n'
    _conn.send(body.encode('utf-8'))
    print(body)


'''Обработчик логов для интроскопа:
отслеживает появление и название нового файла в папке архива'''
def tcp_logger_introscopy(tcp_socket):
    print('TCP Logger started!')

    path_to_archive = os.path.abspath(r'D:/BagVision/Client/Archive')
    now = datetime.datetime.now()
    year = now.strftime('%Y')
    month = now.strftime('%m')
    if int(month) < 10:
        month = month[1]
    day = now.strftime('%d')
    if int(day) < 10:
        day = day[1]
    path_to_archive = path_to_archive + '\\' + year
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_year is already exist')
        pass
    path_to_archive = path_to_archive + '\\' + month
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_month is already exist')
        pass
    path_to_archive = path_to_archive + '\\' + day
    try:
        os.mkdir(path_to_archive, mode=0o777)
    except OSError:
        # print('Dir_day is already exist')
        pass

    '''Включаем отслеживание состояния файлов в папке архива
        на изменение имен и количества файлов по ..FILE_NAME'''
    change_handle_SXR = win32file.FindFirstChangeNotification(
        path_to_archive,
        0,
        win32con.FILE_NOTIFY_CHANGE_FILE_NAME)

    try:
        old_files = dict([(f, None) for f in os.listdir(path_to_archive)])

        while True:
            alarm_state = 0
            adding_state = win32event.WaitForSingleObject(change_handle_SXR, 50)

            '''Check if there are new files in folder'''
            if adding_state == 0:

                new_files = dict([(f, None) for f in os.listdir(path_to_archive)])
                added = [f for f in new_files if f not in old_files]
                added_new = []
                deleted = [f for f in old_files if f not in new_files]
                old_files = new_files
                win32file.FindNextChangeNotification(change_handle_SXR)
                if added:
                    if 'Dangerous' in added[0]:
                        time.sleep(wait_time)
                        alarm_state = 1
                    else:
                        time.sleep(wait_time)
                        '''For BagScan version 6.6 and higher check metadata'''
                        new_sxr_path = path_to_archive + '\\' + added[0]
                        sxr_object = sxr_decode.SXR(new_sxr_path)
                        ver = sxr_object.check_bagscan_version()[1:-2]
                        ver = ver.decode('utf-8').split('.')
                        bagscan_version = ver[0] + ver[1] + '.' + ver[2] + ver[3]
                        if float(bagscan_version) > 66.0:
                            marked_sxr_path = sxr_object.check_metadata()
                            added_new = [marked_sxr_path]
                        else:
                            new_files = dict([(f, None) for f in os.listdir(path_to_archive)])
                            added_new = [f for f in new_files if f not in old_files]

                    '''True when new file was extended with [Dangerous]
                        It means that the alarm button was pressed'''
                    if added_new and 'Dangerous' in added_new[0]:
                        # path_to_new_image = path_to_archive + '\\' + added_new[0]
                        alarm_state = 1
                    # path_to_new_image = path_to_archive + '\\' + added[0]
                    send_message(tcp_socket, alarm_state, 'Introscopy')

                elif deleted:
                    pass

                else:
                    pass

    finally:
        win32file.FindCloseChangeNotification(change_handle_SXR)


'''Обработчик логов для рамки:
получает и обрабатывает данные с COM-порт'''
def tcp_logger_trio(tcp_socket, source='Logger'):
    print('TCP Logger started!')
    alarm_dict = dict()
    alarm_dict['level'] = 0
    alarm_dict['zone'] = []
    alarm_dict['rad_level'] = 0
    alarm_dict['rad_zone'] = []
    alarm_dict['explosives'] = 0
    alarm_dict['time'] = get_int_time()
    while True:
        previous_alarm_dict = alarm_dict
        alarm_dict = global_alarm_dict
        if alarm_dict != previous_alarm_dict:
            send_message(tcp_socket, alarm_dict, 'Trio')
        # else:
        #     print(alarm_message_buff)
        time.sleep(1)


def reset_alarm_dict():
    alarm_dict = dict()
    alarm_dict['level'] = 0
    alarm_dict['zone'] = []
    alarm_dict['rad_level'] = 0
    alarm_dict['rad_zone'] = []
    alarm_dict['explosives'] = 0
    alarm_dict['time'] = get_int_time()
    print('Alarm dict reseted!')
    return alarm_dict


'''Обработчик логов для рамки:
получает и обрабатывает данные с COM-порт
работает фоновым процессом и пишет словарь в global_alarm_dict'''
def tcp_logger_trio_thread():
    alarm_dict = reset_alarm_dict()

    '''получение сообщения от модуля, который слушает COM-порт'''
    alarm_message_buff = serial_port_receiver.serve_serial_port(com_port)
    alarm_message = []
    if type(alarm_message_buff) == bytes:
        for index, value in enumerate(alarm_message_buff):
            buff = struct.unpack('B', alarm_message_buff[index:index+1])[0]
            alarm_message.append(buff)

        '''при получении тревоги входим в эту ветку'''
        if hex(alarm_message[0]) == '0xcc':
            alarm_dict['level'] = alarm_message[1]
            alarm_dict['rad_level'] = alarm_message[2]
            alarm_dict['explosives'] = alarm_message[3]
            alarm_dict['zone'] = []
            alarm_dict['rad_zone'] = []
            if alarm_dict['explosives'] > 0 and alarm_dict['level'] == 0 and alarm_dict['rad_level'] == 0:
                pass
            else:
                for i in range(33):
                    xy_input = i + 4
                    column = i // 11
                    xy_output = str(column + 1) + str(i + 1 - column * 11)
                    if alarm_message[xy_input] == 1:
                        alarm_dict['zone'].append(int(xy_output))
                    elif alarm_message[xy_input] == 2:
                        alarm_dict['rad_zone'].append(int(xy_output))
                    elif alarm_message[xy_input] == 3:
                        alarm_dict['zone'].append(int(xy_output))
                        alarm_dict['rad_zone'].append(int(xy_output))
        # if source == 'Logger':
        #     send_message(tcp_socket, alarm_dict, 'Trio')
        # else:
        #     # return alarm_dict
        #     global global_alarm_dict
        #     global_alarm_dict = alarm_dict
        global global_alarm_dict
        print('Thread Metal:', alarm_dict['level'])
        global_alarm_dict = alarm_dict
    else:
        print(alarm_message_buff)
    # time.sleep(5)


def serve_tcp_logger(tcp_socket, device):
    if device == 'Introscopy':
        tcp_logger_introscopy(tcp_socket)
    elif device == 'Trio':
        tcp_logger_trio(tcp_socket, 'Logger')
    else:
        print('Device type not recognized')


if __name__ == '__main__':
    host = '10.101.63.45'
    port = 23
    device_type = 'Introscopy'
    # device_type = 'Trio'
    serve_tcp_logger(tcp_init(host, port), device_type)
